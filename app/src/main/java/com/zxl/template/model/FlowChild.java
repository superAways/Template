package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

/*
 * @created by LoveLing on 2018/12/28
 * @emil 1079112877@qq.com
 * description:
 */
public class FlowChild extends BaseModel {


    /**
     * special_user_id : null
     * p_index : 10
     * oper_type : 1
     * done_btn : 传阅结束
     * opinion_position : 0
     * flow_name : 传阅
     * special_state : 0
     * child_flow_id : 50227821037a4d8c874f6a06a9a5cefe
     * type : 1
     */

    @JSONField(name = "special_user_id")
    public Object specialUserId;
    @JSONField(name = "p_index")
    public int pIndex;
    @JSONField(name = "oper_type")
    public int operType;
    @JSONField(name = "done_btn")
    public String doneBtn;
    @JSONField(name = "opinion_position")
    public int opinionPosition;
    @JSONField(name = "flow_name")
    public String flowName;
    @JSONField(name = "special_state")
    public String specialState;
    @JSONField(name = "child_flow_id")
    public String childFlowId;
    @JSONField(name = "type")
    public int type;
}
