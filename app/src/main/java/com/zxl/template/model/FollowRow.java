package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

/*
 * @created by LoveLing on 2018/12/6
 * @emil 1079112877@qq.com
 * description:
 */
public class FollowRow extends BaseModel {


    /**
     * handle_id : f85cf3d533fa4288b6748a62aaae65a4
     * is_reback : 0
     * create_time : 2018-09-19 17:31:45
     * doc_title : 邀请函（产业扶贫专题培训）
     * oper_name : 杨李
     * end_time : 2018-09-20 16:29:41
     * document_id : 3b007116cf2b4952a4e3eb8d77d33b36
     * title : 领导批示
     * is_show : 1
     * opinion : 已阅。
     * is_child : 0
     * reback_reason : null
     * flow_type : 2
     * user_id : 0cd35da023054affba6c15220f9bdbb5
     * sender : 收文办理石云天
     * flow_id : e5fca6d704224f41bcbd4720d0ed920f
     * p_id : f611277c27894dda881494cb812f0d09
     * child_finish : 0
     * cx_index : 0
     * status : 1
     * document_type : 1
     */

    @JSONField(name = "handle_id")
    public String handleId;
    @JSONField(name = "is_reback")
    public int isReback;
    @JSONField(name = "create_time")
    public String createTime;
    @JSONField(name = "doc_title")
    public String docTitle;
    @JSONField(name = "oper_name")
    public String operName;
    @JSONField(name = "end_time")
    public String endTime;
    @JSONField(name = "document_id")
    public String documentId;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "is_show")
    public int isShow;
    @JSONField(name = "opinion")
    public String opinion;
    @JSONField(name = "is_child")
    public int isChild;
    @JSONField(name = "reback_reason")
    public Object rebackReason;
    @JSONField(name = "flow_type")
    public int flowType;
    @JSONField(name = "user_id")
    public String userId;
    @JSONField(name = "sender")
    public String sender;
    @JSONField(name = "flow_id")
    public String flowId;
    @JSONField(name = "p_id")
    public String pId;
    @JSONField(name = "child_finish")
    public int childFinish;
    @JSONField(name = "cx_index")
    public int cxIndex;
    @JSONField(name = "status")
    public int status;
    @JSONField(name = "document_type")
    public int documentType;
}
