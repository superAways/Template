package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

/*
 * @created by LoveLing on 2018/12/2
 * @emil 1079112877@qq.com
 * description:
 */
public class Opinion extends BaseModel {

    /**
     * create_time : 2018-10-12 17:56:56
     * user_id : 80248aa8534a433aa16d65451d6a8ab2
     * common_words_id : 4b331305e68040138b6921325fcce97a
     * words : dddd
     */

    @JSONField(name = "create_time")
    public String createTime;
    @JSONField(name = "user_id")
    public String userId;
    @JSONField(name = "common_words_id")
    public String commonWordsId;
    @JSONField(name = "words")
    public String words;


}
