package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

/*
 * @created by LoveLing on 2018/12/6
 * @emil 1079112877@qq.com
 * description:
 */
public class SearchRow extends BaseModel {

    /**
     * reason : 测试
     * document_word_number_id : null
     * key_word : null
     * document_id : e7254655bd564a069c8c0c2459b76422
     * chrdjcsbm : null
     * is_complete_limit : 0
     * special_attach : 0
     * is_report : 0
     * qfrq : null
     * create_time : 2018-12-01 07:26:27
     * issue : 1
     * word1 : 2018
     * document_style : null
     * word3 : 380
     * word2 : 1
     * document_word : 123123
     * gwzh : null
     * dense : 内部
     * user_id : 80248aa8534a433aa16d65451d6a8ab2
     * from_unit : CCTV-7特别节目中心
     * org_id : 2
     * countersign_opinion : null
     * status : 0
     * classify : null
     * retreat_time : null
     * ldps : null
     * issue_opinion : null
     * report_status : 1
     * nianhao : 2018
     * title : <span style="color:red">测试</span>哈哈哈哈gd
     * child_flow_status : 0
     * involve_people : null
     * to_date : 2018-12-01
     * document_num : 1
     * is_del : 0
     * fwsm : null
     * intgwlsh : null
     * report_user_id : null
     * document_type : 1
     * agency : null
     * drawer : null
     * cc_unit : null
     * abstract : 测试
     * attachList : [{"attach_id":"9e0cf386417d4a45a1046ab921f74e65","attach_name":"201812011524261159b76.gd","attach_original_name":"关于开展党员干部在线学习及考试的通知.gd"}]
     * user_cn_name : 超级管理员
     * emergency_degree : 一般
     * nbyj : null
     * complete_limit_time : null
     * report_remark : null
     * qfr : null
     * main_delivery_unit : null
     * current_flow_node : 1
     */

    @JSONField(name = "reason")
    public String reason;
    @JSONField(name = "document_word_number_id")
    public Object documentWordNumberId;
    @JSONField(name = "key_word")
    public Object keyWord;
    @JSONField(name = "document_id")
    public String documentId;
    @JSONField(name = "chrdjcsbm")
    public Object chrdjcsbm;
    @JSONField(name = "is_complete_limit")
    public int isCompleteLimit;
    @JSONField(name = "special_attach")
    public int specialAttach;
    @JSONField(name = "is_report")
    public int isReport;
    @JSONField(name = "qfrq")
    public Object qfrq;
    @JSONField(name = "create_time")
    public String createTime;
    @JSONField(name = "issue")
    public String issue;
    @JSONField(name = "word1")
    public String word1;
    @JSONField(name = "document_style")
    public Object documentStyle;
    @JSONField(name = "word3")
    public String word3;
    @JSONField(name = "word2")
    public String word2;
    @JSONField(name = "document_word")
    public String documentWord;
    @JSONField(name = "gwzh")
    public Object gwzh;
    @JSONField(name = "dense")
    public String dense;
    @JSONField(name = "user_id")
    public String userId;
    @JSONField(name = "from_unit")
    public String fromUnit;
    @JSONField(name = "org_id")
    public String orgId;
    @JSONField(name = "countersign_opinion")
    public Object countersignOpinion;
    @JSONField(name = "status")
    public int status;
    @JSONField(name = "classify")
    public Object classify;
    @JSONField(name = "retreat_time")
    public Object retreatTime;
    @JSONField(name = "ldps")
    public Object ldps;
    @JSONField(name = "issue_opinion")
    public Object issueOpinion;
    @JSONField(name = "report_status")
    public int reportStatus;
    @JSONField(name = "nianhao")
    public String nianhao;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "child_flow_status")
    public int childFlowStatus;
    @JSONField(name = "involve_people")
    public Object involvePeople;
    @JSONField(name = "to_date")
    public String toDate;
    @JSONField(name = "document_num")
    public String documentNum;
    @JSONField(name = "is_del")
    public int isDel;
    @JSONField(name = "fwsm")
    public Object fwsm;
    @JSONField(name = "intgwlsh")
    public Object intgwlsh;
    @JSONField(name = "report_user_id")
    public Object reportUserId;
    @JSONField(name = "document_type")
    public int documentType;
    @JSONField(name = "agency")
    public Object agency;
    @JSONField(name = "drawer")
    public Object drawer;
    @JSONField(name = "cc_unit")
    public Object ccUnit;
    @JSONField(name = "abstract")
    public String abstractX;
    @JSONField(name = "attachList")
    public String attachList;
    @JSONField(name = "user_cn_name")
    public String userCnName;
    @JSONField(name = "emergency_degree")
    public String emergencyDegree;
    @JSONField(name = "nbyj")
    public Object nbyj;
    @JSONField(name = "complete_limit_time")
    public Object completeLimitTime;
    @JSONField(name = "report_remark")
    public Object reportRemark;
    @JSONField(name = "qfr")
    public Object qfr;
    @JSONField(name = "main_delivery_unit")
    public Object mainDeliveryUnit;
    @JSONField(name = "current_flow_node")
    public String currentFlowNode;
}
