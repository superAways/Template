package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

/*
 * @created by LoveLing on 2018/12/6
 * @emil 1079112877@qq.com
 * description:
 */
public class FollowGroupUser extends BaseModel {


    /**
     * fid : 80248aa8534a433aa16d65451d6a8ab2
     * id : 0cd35da023054affba6c15220f9bdbb5
     * text : 杨李
     */

    @JSONField(name = "fid")
    public String fid;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "text")
    public String text;
}
