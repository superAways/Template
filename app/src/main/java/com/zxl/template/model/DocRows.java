package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

/**
 * author : 王新海
 * time   : 2018/11/28.
 * version: 1.0.0
 * desc   :
 */
public class DocRows extends BaseModel {

    /**
     * reason : null
     * document_word_number_id : null
     * key_word : null
     * document_id : e7a4d7e224e441ae88f9e00de9863617
     * chrdjcsbm : null
     * is_complete_limit : 1
     * flow_id : b385691a747f49338355ae3575e060e5
     * special_attach : 0
     * is_report : 0
     * qfrq : null
     * handle_title : 拟文登记
     * create_time : 2018-11-28 13:18:49
     * issue : null
     * word1 : null
     * document_style : 会议纪要
     * word3 : null
     * word2 : null
     * document_word : 云开组办发电
     * gwzh : null
     * dense : 内部
     * send_time : 2018-11-28 13:18:49
     * user_id : 80248aa8534a433aa16d65451d6a8ab2
     * sender : 超级管理员
     * from_unit : null
     * org_id : 2
     * countersign_opinion : null
     * status : 0
     * handle_id : 109dcc07237c4472bd7f9971eab6a132
     * classify : 行政公文
     * retreat_time : null
     * ldps : null
     * issue_opinion : null
     * report_status : 1
     * nianhao : 2018
     * title : 12312
     * child_flow_status : 0
     * involve_people : null
     * to_date : null
     * document_num : 1
     * is_del : 0
     * fwsm : null
     * intgwlsh : null
     * report_user_id : null
     * document_type : 2
     * agency : 云南省扶贫办
     * drawer : 超级管理员
     * cc_unit : 123123
     * abstract : null
     * user_cn_name : 超级管理员
     * emergency_degree : 一般
     * nbyj : null
     * complete_limit_time : 2018-11-28
     * report_remark : null
     * qfr : null
     * main_delivery_unit : 12312312
     * current_flow_node : b385691a747f49338355ae3575e060e5
     */

    @JSONField(name = "reason")
    public Object reason;
    @JSONField(name = "document_word_number_id")
    public Object documentWordNumberId;
    @JSONField(name = "key_word")
    public Object keyWord;
    @JSONField(name = "document_id")
    public String documentId;
    @JSONField(name = "chrdjcsbm")
    public Object chrdjcsbm;
    @JSONField(name = "is_complete_limit")
    public int isCompleteLimit;
    @JSONField(name = "flow_id")
    public String flowId;
    @JSONField(name = "special_attach")
    public int specialAttach;
    @JSONField(name = "is_report")
    public int isReport;
    @JSONField(name = "qfrq")
    public Object qfrq;
    @JSONField(name = "handle_title")
    public String handleTitle;
    @JSONField(name = "create_time")
    public String createTime;
    @JSONField(name = "issue")
    public Object issue;
    @JSONField(name = "word1")
    public Object word1;
    @JSONField(name = "document_style")
    public String documentStyle;
    @JSONField(name = "word3")
    public Object word3;
    @JSONField(name = "word2")
    public Object word2;
    @JSONField(name = "document_word")
    public String documentWord;
    @JSONField(name = "gwzh")
    public String gwzh;
    @JSONField(name = "dense")
    public String dense;
    @JSONField(name = "send_time")
    public String sendTime;
    @JSONField(name = "user_id")
    public String userId;
    @JSONField(name = "sender")
    public String sender;
    @JSONField(name = "from_unit")
    public String fromUnit;
    @JSONField(name = "org_id")
    public String orgId;
    @JSONField(name = "countersign_opinion")
    public Object countersignOpinion;
    @JSONField(name = "status")
    public int status;
    @JSONField(name = "handle_id")
    public String handleId;
    @JSONField(name = "classify")
    public String classify;
    @JSONField(name = "retreat_time")
    public Object retreatTime;
    @JSONField(name = "ldps")
    public Object ldps;
    @JSONField(name = "issue_opinion")
    public Object issueOpinion;
    @JSONField(name = "report_status")
    public int reportStatus;
    @JSONField(name = "nianhao")
    public String nianhao;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "child_flow_status")
    public int childFlowStatus;
    @JSONField(name = "involve_people")
    public Object involvePeople;
    @JSONField(name = "to_date")
    public Object toDate;
    @JSONField(name = "document_num")
    public String documentNum;
    @JSONField(name = "is_del")
    public int isDel;
    @JSONField(name = "fwsm")
    public Object fwsm;
    @JSONField(name = "intgwlsh")
    public Object intgwlsh;
    @JSONField(name = "report_user_id")
    public Object reportUserId;
    @JSONField(name = "document_type")
    public int documentType;
    @JSONField(name = "agency")
    public String agency;
    @JSONField(name = "drawer")
    public String drawer;
    @JSONField(name = "cc_unit")
    public String ccUnit;
    @JSONField(name = "abstract")
    public Object abstractX;
    @JSONField(name = "user_cn_name")
    public String userCnName;
    @JSONField(name = "emergency_degree")
    public String emergencyDegree;
    @JSONField(name = "nbyj")
    public Object nbyj;
    @JSONField(name = "complete_limit_time")
    public String completeLimitTime;
    @JSONField(name = "report_remark")
    public Object reportRemark;
    @JSONField(name = "qfr")
    public Object qfr;
    @JSONField(name = "main_delivery_unit")
    public String mainDeliveryUnit;
    @JSONField(name = "current_flow_node")
    public String currentFlowNode;


    /**
     * handle_name : 拟文登记
     * oper_name : 超级管理员
     * end_time : 2018-07-22 13:29:06
     * complete_limit_time : null
     * handle_status : 1
     * gwzh : null
     */

    @JSONField(name = "handle_name")
    public String handleName;
    @JSONField(name = "oper_name")
    public String operName;
    @JSONField(name = "end_time")
    public String endTime;
    @JSONField(name = "handle_status")
    public int handleStatus;
    @JSONField(name = "gwzh")
    public Object gwzhX;


}
