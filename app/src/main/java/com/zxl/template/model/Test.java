package com.zxl.template.model;

import android.support.annotation.NonNull;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

/*
 * @created by LoveLing on 2018/11/12
 * @emil 1079112877@qq.com
 * description:
 */
public class Test extends BaseModel {
    public Test(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "name")
    public String name;

    @NonNull
    @Override
    public String toString() {
        return name;
    }
}
