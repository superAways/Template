package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

import java.util.List;

/*
 * @created by LoveLing on 2018/11/22
 * @emil 1079112877@qq.com
 * description:
 */
public class Way extends BaseModel {
    @JSONField(name = "way_name")
    public String wayName;
    @JSONField(name = "way_id")
    public int wayId;
    @JSONField(name = "personnel_list")
    public List<Operator> personList;

    public Way(String wayName, int wayId, List<Operator> personList) {
        this.wayName = wayName;
        this.wayId = wayId;
        this.personList = personList;
    }

}
