package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

/**
 * author : 王新海
 * time   : 2018/11/25.
 * version: 1.0.0
 * desc   :
 */
public class Operator extends BaseModel {
    @JSONField(name = "operator_name")
    public String operatorName;
    @JSONField(name = "operator_id")
    public int operatorId;
    @JSONField(name = "operator_phone")
    public String operatorPhone;
    @JSONField(name = "operator_tel")
    public String operatorTel;

    public boolean isChecked = false;


    public Operator(String operatorName, int operatorId, String operatorPhone, String operatorTel) {
        this.operatorName = operatorName;
        this.operatorId = operatorId;
        this.operatorPhone = operatorPhone;
        this.operatorTel = operatorTel;
    }
}
