package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

/*
 * @created by LoveLing on 2018/12/28
 * @emil 1079112877@qq.com
 * description: 领导批示
 */
public class LDYJ extends BaseModel {


    /**
     * handle_id : 6fa3d0bfd3d74a648ce6f2be0eccd510
     * special_user_id : null
     * opinion_position : 2
     * document_id : d3a044c0d4804e2b92173930e9290de7
     * title : 领导批示
     * type : 1
     * is_child : 0
     * reback_reason : null
     * p_index : 1
     * flow_id : e5fca6d704224f41bcbd4720d0ed920f
     * child_finish : 0
     * cx_index : 0
     * is_reback : 0
     * create_time : 2018-12-27 17:08:04
     * done_btn : 批示结束
     * oper_name : 黄建国
     * end_time : 2018-12-28 08:09:19
     * flow_name : 领导批示
     * special_state : 0
     * child_flow_id : e5fca6d704224f41bcbd4720d0ed920f
     * is_show : 1
     * opinion : 全局人员传阅，确保不出问题。综合处办理相关事宜。节日教育时学习传达。
     * user_cn_name : 黄建国
     * flow_type : 2
     * user_id : 33c392e3b8a14dc3ac1050bcba0aef7f
     * sender : 办文人员(张艳惠)
     * oper_type : 3
     * org_id : 5
     * p_id : 6dc733141da94cb0b7b6e8e61c958a7f
     * status : 1
     */

    @JSONField(name = "handle_id")
    public String handleId;
    @JSONField(name = "special_user_id")
    public Object specialUserId;
    @JSONField(name = "opinion_position")
    public int opinionPosition;
    @JSONField(name = "document_id")
    public String documentId;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "type")
    public int type;
    @JSONField(name = "is_child")
    public int isChild;
    @JSONField(name = "reback_reason")
    public Object rebackReason;
    @JSONField(name = "p_index")
    public int pIndex;
    @JSONField(name = "flow_id")
    public String flowId;
    @JSONField(name = "child_finish")
    public int childFinish;
    @JSONField(name = "cx_index")
    public int cxIndex;
    @JSONField(name = "is_reback")
    public int isReback;
    @JSONField(name = "create_time")
    public String createTime;
    @JSONField(name = "done_btn")
    public String doneBtn;
    @JSONField(name = "oper_name")
    public String operName;
    @JSONField(name = "end_time")
    public String endTime;
    @JSONField(name = "flow_name")
    public String flowName;
    @JSONField(name = "special_state")
    public String specialState;
    @JSONField(name = "child_flow_id")
    public String childFlowId;
    @JSONField(name = "is_show")
    public int isShow;
    @JSONField(name = "opinion")
    public String opinion;
    @JSONField(name = "user_cn_name")
    public String userCnName;
    @JSONField(name = "flow_type")
    public int flowType;
    @JSONField(name = "user_id")
    public String userId;
    @JSONField(name = "sender")
    public String sender;
    @JSONField(name = "oper_type")
    public int operType;
    @JSONField(name = "org_id")
    public String orgId;
    @JSONField(name = "p_id")
    public String pId;
    @JSONField(name = "status")
    public int status;
}
