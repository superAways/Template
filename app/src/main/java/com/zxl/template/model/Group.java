package com.zxl.template.model;

import android.support.annotation.NonNull;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

/*
 * @created by LoveLing on 2018/12/5
 * @emil 1079112877@qq.com
 * description:
 */
public class Group extends BaseModel {

    /**
     * user_cn_name : 超级管理员
     * create_time : 2018-10-10 16:10:54
     * collect_group_id : 49c339a6aaf142a3a7c83885574189cf
     * group_name : 11111
     * user_id : 80248aa8534a433aa16d65451d6a8ab2
     * org_id : 2
     * is_del : 0
     * username : admin
     */

    @JSONField(name = "user_cn_name")
    public String userCnName;
    @JSONField(name = "create_time")
    public String createTime;
    @JSONField(name = "collect_group_id")
    public String collectGroupId;
    @JSONField(name = "group_name")
    public String groupName;
    @JSONField(name = "user_id")
    public String userId;
    @JSONField(name = "org_id")
    public String orgId;
    @JSONField(name = "is_del")
    public int isDel;
    @JSONField(name = "username")
    public String username;

    @NonNull
    @Override
    public String toString() {
        return groupName;
    }
}
