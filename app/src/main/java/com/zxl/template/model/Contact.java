package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

import java.util.List;

/*
 * @created by LoveLing on 2018/11/29
 * @emil 1079112877@qq.com
 * description:
 */
public class Contact extends BaseModel {

    @JSONField(name = "rows")
    public List<ContactRows> rows;



}
