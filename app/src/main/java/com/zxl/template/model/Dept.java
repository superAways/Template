package com.zxl.template.model;

import android.support.annotation.NonNull;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

import java.util.List;

/*
 * @created by LoveLing on 2018/11/29
 * @emil 1079112877@qq.com
 * description:机构
 */
public class Dept extends BaseModel {

    @JSONField(name = "fid")
    public String fid;
    @JSONField(name = "org_is_del")
    public int orgIsDel;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "text")
    public String text;


    //下级机构
    public List<Dept> subDept;

    @NonNull
    @Override
    public String toString() {
        return text;
    }
}
