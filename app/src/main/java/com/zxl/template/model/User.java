package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * author : 王新海
 * time   : 2018/11/28.
 * version: 1.0.0
 * desc   :
 */
public class User {

    @JSONField(name = "cellpone_tel")
    public String cellponeTel;
    @JSONField(name = "my_concern")
    public String myConcern;
    @JSONField(name = "org_name")
    public String orgName;
    @JSONField(name = "user_cn_name")
    public String userCnName;
    @JSONField(name = "office_tel")
    public String officeTel;
    @JSONField(name = "org_id")
    public String orgId;

}
