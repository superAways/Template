package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

/*
 * @created by LoveLing on 2018/12/4
 * @emil 1079112877@qq.com
 * description:
 */
public class DocParams extends BaseModel {


    /**
     * flow_type : 1
     * flow_id : 1
     * flow_name : 收文办理
     * nums : 13
     */

    @JSONField(name = "flow_type")
    public int flowType;
    @JSONField(name = "flow_id")
    public String flowId;
    @JSONField(name = "flow_name")
    public String flowName;
    @JSONField(name = "nums")
    public int nums;

    public Boolean isSwitch;

}
