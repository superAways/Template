package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

/*
 * @created by LoveLing on 2018/12/5
 * @emil 1079112877@qq.com
 * description:
 */
public class CollectRow extends BaseModel {

    /**
     * reason : 测试
     * classify : null
     * ldps : null
     * issue_opinion : null
     * nianhao : 2018
     * key_word : null
     * title : 测试哈哈哈哈gd
     * document_id : e7254655bd564a069c8c0c2459b76422
     * child_flow_status : 0
     * involve_people : null
     * is_complete_limit : 0
     * to_date : 2018-12-01
     * collect_group_id : 49c339a6aaf142a3a7c83885574189cf
     * document_num : 1
     * special_attach : 0
     * document_type : 1
     * create_time : 2018-12-01 07:26:27
     * issue : 1
     * agency : null
     * word1 : 2018
     * document_style : null
     * word3 : 380
     * word2 : 1
     * drawer : null
     * cc_unit : null
     * abstract : 测试
     * document_word : 123123
     * dense : 内部
     * emergency_degree : 一般
     * user_id : 80248aa8534a433aa16d65451d6a8ab2
     * from_unit : CCTV-7特别节目中心
     * nbyj : null
     * complete_limit_time : null
     * countersign_opinion : null
     * main_delivery_unit : null
     * status : 0
     * current_flow_node : 1
     */

    @JSONField(name = "reason")
    public String reason;
    @JSONField(name = "classify")
    public Object classify;
    @JSONField(name = "ldps")
    public Object ldps;
    @JSONField(name = "issue_opinion")
    public Object issueOpinion;
    @JSONField(name = "nianhao")
    public String nianhao;
    @JSONField(name = "key_word")
    public Object keyWord;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "document_id")
    public String documentId;
    @JSONField(name = "child_flow_status")
    public int childFlowStatus;
    @JSONField(name = "involve_people")
    public Object involvePeople;
    @JSONField(name = "is_complete_limit")
    public int isCompleteLimit;
    @JSONField(name = "to_date")
    public String toDate;
    @JSONField(name = "collect_group_id")
    public String collectGroupId;
    @JSONField(name = "document_num")
    public String documentNum;
    @JSONField(name = "special_attach")
    public int specialAttach;
    @JSONField(name = "document_type")
    public int documentType;
    @JSONField(name = "create_time")
    public String createTime;
    @JSONField(name = "issue")
    public String issue;
    @JSONField(name = "agency")
    public Object agency;
    @JSONField(name = "word1")
    public String word1;
    @JSONField(name = "document_style")
    public Object documentStyle;
    @JSONField(name = "word3")
    public String word3;
    @JSONField(name = "word2")
    public String word2;
    @JSONField(name = "drawer")
    public Object drawer;
    @JSONField(name = "cc_unit")
    public Object ccUnit;
    @JSONField(name = "abstract")
    public String abstractX;
    @JSONField(name = "document_word")
    public String documentWord;
    @JSONField(name = "dense")
    public String dense;
    @JSONField(name = "emergency_degree")
    public String emergencyDegree;
    @JSONField(name = "user_id")
    public String userId;
    @JSONField(name = "from_unit")
    public String fromUnit;
    @JSONField(name = "nbyj")
    public Object nbyj;
    @JSONField(name = "complete_limit_time")
    public Object completeLimitTime;
    @JSONField(name = "countersign_opinion")
    public Object countersignOpinion;
    @JSONField(name = "main_delivery_unit")
    public Object mainDeliveryUnit;
    @JSONField(name = "status")
    public int status;
    @JSONField(name = "current_flow_node")
    public String currentFlowNode;
}
