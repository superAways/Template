package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

import java.util.List;

/*
 * @created by LoveLing on 2018/12/1
 * @emil 1079112877@qq.com
 * description:
 */
public class Receipt extends BaseModel {

    @JSONField(name = "flowMain")
    public FlowMain flowMain;
    @JSONField(name = "to_date")
    public String toDate;
    @JSONField(name = "is_complete_limit")
    public int isCompleteLimit;
    @JSONField(name = "from_unit")
    public String fromUnit;
    @JSONField(name = "complete_limit_time")
    public String completeLimitTime;
    @JSONField(name = "handle")
    public Handle handle;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "word")
    public String word;
    @JSONField(name = "fujiangaos")
    public List<DocFile> fjDocFiles;
    @JSONField(name = "zhengwengaos")
    public List<DocFile> zwDocFiles;
    @JSONField(name = "childFlows")
    public List<ChildFlows> childFlows;
    @JSONField(name = "commonWords")
    public List<Opinion> opinions;

    @JSONField(name = "ldyjList")
    public List<LDYJ> ldyjs;
    @JSONField(name = "nbyj")
    public String nbyj;
    @JSONField(name = "swblyj")
    public String swblyj;
    @JSONField(name = "flowChild")
    public FlowChild flowChild;

    /**
     * iszwgk : 1
     * agency : 云南省扶贫办
     * drawer : 超级管理员
     * main_delivery_unit : 12312312
     */

    @JSONField(name = "iszwgk")
    public int iszwgk; //公开属性 0此件不公开 1此件公开发布 2此件依申请公开 3此件删减后公开
    @JSONField(name = "agency")
    public String agency;
    @JSONField(name = "drawer")
    public String drawer;
    @JSONField(name = "main_delivery_unit")
    public String mainDeliveryUnit;
    @JSONField(name = "create_time")
    public String createTime;

    public static class FlowMain {

        @JSONField(name = "is_rewriting")
        public int isRewriting;
        @JSONField(name = "pre_id")
        public String preId;
        @JSONField(name = "color")
        public String color;
        @JSONField(name = "done_btn")
        public String doneBtn;
        @JSONField(name = "icon")
        public String icon;
        @JSONField(name = "document_id")
        public String documentId;
        @JSONField(name = "type")
        public String type;
        @JSONField(name = "title")
        public String title;
        @JSONField(name = "line_height")
        public String lineHeight;
        @JSONField(name = "top")
        public String top;
        @JSONField(name = "left")
        public String left;
        @JSONField(name = "is_handle")
        public int isHandle;
        @JSONField(name = "width")
        public String width;
        @JSONField(name = "main_flow_document_id")
        public String mainFlowDocumentId;
        @JSONField(name = "main_flow_id")
        public String mainFlowId;
        @JSONField(name = "next_id")
        public String nextId;
        @JSONField(name = "height")
        public String height;
    }

    public static class Handle {

        @JSONField(name = "handle_id")
        public String handleId;
        @JSONField(name = "is_reback")
        public int isReback;
        @JSONField(name = "create_time")
        public String createTime;
        @JSONField(name = "oper_name")
        public String operName;
        @JSONField(name = "end_time")
        public Object endTime;
        @JSONField(name = "document_id")
        public String documentId;
        @JSONField(name = "title")
        public String title;
        @JSONField(name = "is_show")
        public int isShow;
        @JSONField(name = "opinion")
        public Object opinion;
        @JSONField(name = "is_child")
        public int isChild;
        @JSONField(name = "user_cn_name")
        public String userCnName;
        @JSONField(name = "reback_reason")
        public Object rebackReason;
        @JSONField(name = "flow_type")
        public int flowType;
        @JSONField(name = "user_id")
        public String userId;
        @JSONField(name = "sender")
        public String sender;
        @JSONField(name = "flow_id")
        public String flowId;
        @JSONField(name = "org_id")
        public String orgId;
        @JSONField(name = "p_id")
        public String pId;
        @JSONField(name = "child_finish")
        public int childFinish;
        @JSONField(name = "status")
        public int status;
        @JSONField(name = "cx_index")
        public int cxIndex;
    }

    public static class ChildFlows {

        @JSONField(name = "special_user_id")
        public Object specialUserId;
        @JSONField(name = "p_index")
        public int pIndex;
        @JSONField(name = "oper_type")
        public int operType;
        @JSONField(name = "done_btn")
        public String doneBtn;
        @JSONField(name = "opinion_position")
        public int opinionPosition;
        @JSONField(name = "flow_name")
        public String flowName;
        @JSONField(name = "special_state")
        public String specialState;
        @JSONField(name = "child_flow_id")
        public String childFlowId;
        @JSONField(name = "type")
        public int type;

    }
}
