package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

/*
 * @created by LoveLing on 2018/12/2
 * @emil 1079112877@qq.com
 * description:
 */
public class FlowDetail extends BaseModel {


    /**
     * handle_id : c818800783f64062a2d99fe8e99931d8
     * is_reback : 0
     * create_time : 2018-12-01 03:59:30
     * oper_name : 超级管理员
     * end_time : null
     * document_id : 257ad011771e4b7782864d3919fc7c38
     * title : 收文办理
     * is_show : 1
     * opinion : null
     * is_child : 0
     * user_cn_name : 超级管理员
     * reback_reason : null
     * flow_type : 1
     * user_id : 80248aa8534a433aa16d65451d6a8ab2
     * sender : 超级管理员
     * flow_id : 1
     * org_id : 2
     * p_id : 0
     * child_finish : 0
     * status : 0
     * cx_index : 0
     */

    @JSONField(name = "handle_id")
    public String handleId;
    @JSONField(name = "is_reback")
    public int isReback;
    @JSONField(name = "create_time")
    public String createTime;
    @JSONField(name = "oper_name")
    public String operName;
    @JSONField(name = "end_time")
    public Object endTime;
    @JSONField(name = "document_id")
    public String documentId;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "is_show")
    public int isShow;
    @JSONField(name = "opinion")
    public Object opinion;
    @JSONField(name = "is_child")
    public int isChild;
    @JSONField(name = "user_cn_name")
    public String userCnName;
    @JSONField(name = "reback_reason")
    public Object rebackReason;
    @JSONField(name = "flow_type")
    public int flowType;
    @JSONField(name = "user_id")
    public String userId;
    @JSONField(name = "sender")
    public String sender;
    @JSONField(name = "flow_id")
    public String flowId;
    @JSONField(name = "org_id")
    public String orgId;
    @JSONField(name = "p_id")
    public String pId;
    @JSONField(name = "child_finish")
    public int childFinish;
    @JSONField(name = "status")
    public int status;
    @JSONField(name = "cx_index")
    public int cxIndex;
}
