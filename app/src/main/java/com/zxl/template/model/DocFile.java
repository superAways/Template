package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

/*
 * @created by LoveLing on 2018/12/1
 * @emil 1079112877@qq.com
 * description: 文稿
 */
public class DocFile extends BaseModel {
    /**
     * oper_time : 2018-12-01 11:59:29
     * attach_type : 0
     * attach_id : b86ee2232b9846ec88a5ed47c6cb9481
     * document_id : 257ad011771e4b7782864d3919fc7c38
     * is_formal : 1
     * user_cn_name : 超级管理员
     * user_id : 80248aa8534a433aa16d65451d6a8ab2
     * file_type : .docx
     * org_id : 2
     * attach_name : 20181201112459229cbd4.docx
     * is_del : 0
     * attach_original_name : 新建Microsoft Word 文档.docx
     * username : admin
     */

    @JSONField(name = "oper_time")
    public String operTime;
    @JSONField(name = "attach_type")
    public int attachType;
    @JSONField(name = "attach_id")
    public String attachId;
    @JSONField(name = "document_id")
    public String documentId;
    @JSONField(name = "is_formal")
    public int isFormal;
    @JSONField(name = "user_cn_name")
    public String userCnName;
    @JSONField(name = "user_id")
    public String userId;
    @JSONField(name = "file_type")
    public String fileType;
    @JSONField(name = "org_id")
    public String orgId;
    @JSONField(name = "attach_name")
    public String attachName;
    @JSONField(name = "is_del")
    public int isDel;
    @JSONField(name = "attach_original_name")
    public String attachOriginalName;
    @JSONField(name = "username")
    public String username;
}
