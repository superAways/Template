package com.zxl.template.model;

/*
 * @created by LoveLing on 2018/11/24
 * @emil 1079112877@qq.com
 * description:
 */
public class MsgEvent<T> {


    public MsgEvent(String type, T t) {
        this.type = type;
        this.t = t;
    }

    public T t;
    public String type;

}
