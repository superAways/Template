package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

import java.util.List;

/*
 * @created by LoveLing on 2018/11/22
 * @emil 1079112877@qq.com
 * description:
 */
public class Doc extends BaseModel {

    @JSONField(name = "total")
    public int total;
    @JSONField(name = "rows")
    public List<DocRows> rows;
}
