package com.zxl.template.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.zxl.template.base.BaseModel;

/*
 * @created by LoveLing on 2018/11/29
 * @emil 1079112877@qq.com
 * description:
 */
public class ContactRows extends BaseModel {
    /**
     * is_locked : 0
     * user_order : 0
     * cellpone_tel : 13888601025
     * Info : 审核成功
     * user_cn_name : 王鹏
     * role_name : 超级管理员
     * user_role_id : 4ac0db094d6c4aa6bb27b617856b4db6
     * password : $shiro1$SHA-256$500000$x/WnIy/zNE24fMEllhdzPw==$0L70qjOB7N89/3OemKfvePLMp8SxAEJsxzOpqSkCG9o=
     * Stat : 0
     * user_id : 01543661e6a34cc29ceec6e597a495c2
     * org_id : 40f8471cda394726bb5ffc16e2c6c8bb
     * office_tel : null
     * is_del : 0
     * org_name : 收文办理
     * email : null
     * username : wp
     */

    @JSONField(name = "is_locked")
    public int isLocked;
    @JSONField(name = "user_order")
    public int userOrder;
    @JSONField(name = "cellpone_tel")
    public String cellponeTel;
    @JSONField(name = "Info")
    public String Info;
    @JSONField(name = "user_cn_name")
    public String userCnName;
    @JSONField(name = "role_name")
    public String roleName;
    @JSONField(name = "user_role_id")
    public String userRoleId;
    @JSONField(name = "password")
    public String password;
    @JSONField(name = "Stat")
    public int Stat;
    @JSONField(name = "user_id")
    public String userId;
    @JSONField(name = "org_id")
    public String orgId;
    @JSONField(name = "office_tel")
    public String officeTel;
    @JSONField(name = "is_del")
    public int isDel;
    @JSONField(name = "org_name")
    public String orgName;
    @JSONField(name = "email")
    public Object email;
    @JSONField(name = "username")
    public String username;


    //是否选中
    public boolean isCheck = false;

}
