package com.zxl.template.base;

import android.content.res.Configuration;
import android.support.v4.app.Fragment;

import com.zxl.template.App;
import com.zxl.template.http.ApiControl;

/*
 * @created by LoveLing on 2018/11/12
 * @emil 1079112877@qq.com
 * description:
 */
public class BaseFrag extends Fragment {
    public ApiControl mApi = App.mApp.mApi;


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
