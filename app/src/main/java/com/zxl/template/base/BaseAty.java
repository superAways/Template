package com.zxl.template.base;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;

import com.zxl.template.App;
import com.zxl.template.http.ApiControl;

/*
 * @created by LoveLing on 2018/11/12
 * @emil 1079112877@qq.com
 * description:
 */
public class BaseAty extends AppCompatActivity {

    public ApiControl mApi = App.mApp.mApi;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

}
