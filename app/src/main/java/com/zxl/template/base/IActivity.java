package com.zxl.template.base;

/*
 * @created by LoveLing on 2018/11/19
 * @emil 1079112877@qq.com
 * description:
 */
public interface IActivity {

    void initListener();

    void initData();

    void initView();

}
