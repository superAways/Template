package com.zxl.template.broadcast;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.VpnService;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.sslvpn_android_client.VPNServiceManager;
import com.zxl.template.App;
import com.zxl.template.activity.LoginActivity;
import com.zxl.template.activity.MainActivity;
import com.zxl.template.constant.constant;
import com.zxl.template.model.MsgEvent;
import com.zxl.zlibrary.tool.LCacheTool;
import com.zxl.zlibrary.tool.LEmptyTool;
import com.zxl.zlibrary.tool.LToast;
import com.zxl.zlibrary.tool.LogTool;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

/*
 * @created by LoveLing on 2019/1/17
 * @emil 1079112877@qq.com
 * description:
 */
public class NetBroadCastReceiver extends BroadcastReceiver implements constant {
    private static final String TAG = "NetBroadCastReceiver";


    private Context mContext;
    @SuppressLint("SdCardPath")
    private final static String vpnPath = "/data/data/com.zxl.template/vpn";

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;

        if (BROADCAST_VPN_SERVICE.equals(intent.getAction())) {
            String message = intent.getStringExtra(VPNServiceManager.MESSAGE_CODE);
            JSONObject json = JSON.parseObject(message);
            int type = json.getInteger("type");
            int result = json.getInteger("result");
            String reason = json.getString("reason");

            LogTool.e(TAG + "：" + result + "，" + reason + "，" + type);


            switch (result) {
                //登录成功
                case 2001:
                    LToast.success("VPN连接成功！");
                    break;
                //连接异常，请检查网络设置或地址是否正确！
                case 2004:
                    LToast.normal("vpn正在重连", Toast.LENGTH_LONG);
                    initVpn();
                    break;
                //连接异常，请检查网络设置或地址是否正确！
                case 2005:
                    LToast.normal("vpn正在重连", Toast.LENGTH_LONG);
                    initVpn();
                    break;
                //获取虚拟网卡信息失败，请重新登录！
                case 2012:
//                    LToast.normal(reason + ",请重连!", Toast.LENGTH_LONG);
//                    EventBus.getDefault().post(new MsgEvent<>("reVpn", ""));

                    break;
                default:

                    break;
            }

        }
    }

    private void initVpn() {
        EventBus.getDefault().post(new MsgEvent<>("reVpn", ""));

    }
}
