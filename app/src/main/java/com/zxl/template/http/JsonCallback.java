package com.zxl.template.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zxl.template.model.User;
import com.zxl.zlibrary.http.callback.GenericsCallback;
import com.zxl.zlibrary.http.callback.JsonGenericsSerializator;
import com.zxl.zlibrary.tool.LogTool;

import okhttp3.Response;
import okhttp3.ResponseBody;

/*
 * @created by LoveLing on 2018/11/12
 * @emil 1079112877@qq.com
 * description:
 */
public abstract class JsonCallback<T> extends GenericsCallback<T> {
    private static final String TAG = "JsonCallback";


    public JsonCallback() {
        super(new JsonGenericsSerializator());
    }


    @Override
    public String validateReponse(Response response, int id) throws Exception {
        assert response.body() != null;
        String responseBody = response.body().string();
//        responseBody = trim(responseBody);
        JSONObject json = JSON.parseObject(responseBody);

        if (json == null) {
            return ("Response is not json data!");
        } else {
            LogTool.d("response:" + json.toJSONString());

            //以下这一推约定是可以变的，根据后台的结构来弄
            String data = json.getString("o");
            String msg = json.getString("msg");
            String status = json.getString("status");

            if (status.equals("y")) {
                validateData = data;
                return null;
            }
            return msg;
        }
    }

    private String trim(String str) {
        String s = str.replaceAll("\\s*", "");
        System.out.println(s);
        return s;
    }
}
