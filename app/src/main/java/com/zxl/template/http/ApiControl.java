package com.zxl.template.http;

import android.content.Context;

import com.alibaba.fastjson.JSON;
import com.zxl.template.model.User;
import com.zxl.zlibrary.http.OkHttpUtils;
import com.zxl.zlibrary.http.callback.FileCallBack;
import com.zxl.zlibrary.http.callback.StringCallback;
import com.zxl.zlibrary.tool.LEmptyTool;
import com.zxl.zlibrary.tool.LToast;
import com.zxl.zlibrary.tool.LogTool;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.Response;

/*
 * @created by LoveLing on 2018/11/19
 * @emil 1079112877@qq.com
 * description:
 */
public class ApiControl {


    public String API;
    public User mCurrentUser;

    public String username;
    public String password;

    private Context mContext;
    private String url;


    public ApiControl(Context context) {
        this.mContext = context;
    }

    public void initApi(String api) {
        this.API = "http://" + api + "/api/";
    }

    /**
     * 登录接口
     *
     * @param tag
     * @param username
     * @param password
     * @param callback
     */
    public void login(Object tag, String username, String password, LoginCallback callback) {
        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(API + "login/doLogin")
                .build()
                .execute(callback);

    }

    public abstract class LoginCallback extends JsonCallback {
        @Override
        public Object parseNetworkResponse(Response response, int id) {
            mCurrentUser = JSON.parseObject(validateData, User.class);
            return null;
        }
    }

    /**
     * 代办公文
     *
     * @param tag
     * @param page
     * @param rows
     * @param keyword
     * @param document_type 1 收文 2发文
     * @param flow_id
     * @param callback
     */
    public void agencyDocList(Object tag, int page, int rows, String keyword, String document_type, String flow_id, JsonCallback callback) {
        Map<String, String> params = new HashMap<>();
        params.put("page", page + "");
        params.put("rows", rows + "");

        if (LEmptyTool.isNotEmpty(keyword)) {
            params.put("keyword", keyword);
        }

        if (LEmptyTool.isNotEmpty(document_type)) {
            params.put("document_type", document_type);
        }
        if (LEmptyTool.isNotEmpty(flow_id)) {
            params.put("flow_id", flow_id);
        }
        LogTool.d(JSON.toJSONString(params));

        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(url = API + "document/todoListJson")
                .params(params)
                .build()
                .execute(callback);
    }

    /**
     * 已经办理
     *
     * @param tag
     * @param page
     * @param rows
     * @param titleKey
     * @param type
     * @param callback
     */
    public void doDocList(Object tag, int page, int rows, String titleKey, String type, JsonCallback callback) {
        Map<String, String> params = new HashMap<>();
        params.put("page", page + "");
        params.put("rows", rows + "");
        if (LEmptyTool.isNotEmpty(titleKey)) {
            params.put("titleKey", titleKey);
        }
        if (LEmptyTool.isNotEmpty(type)) {
            params.put("type", type);
        }
        LogTool.d(JSON.toJSONString(params));
        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(API + "document/myListJson")
                .params(params)
                .build()
                .execute(callback);
    }

    public void docList(Object tag, int type, JsonCallback callback) {
//        http://132.232.4.78/api/document/todoListJson  待办公文
//        http://132.232.4.78/api/document/myListJson  已办公文
        if (type == 1)
            url = API + "document/todoListJson";
        else
            url = API + "document/myListJson";

        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(url)
                .build()
                .execute(callback);
    }

    /**
     * 机构列表
     *
     * @param tag
     * @param callback
     */
    public void deptList(Object tag, JsonCallback callback) {
//   http://oa.dev.chuyuntech.com/oa/api/org/orgTree
        url = API + "org/orgTree";
        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(url)
                .build()
                .execute(callback);
    }

    /**
     * 获取联系人
     *
     * @param tag
     * @param orgId
     * @param callback
     */
    public void contactList(Object tag, String orgId, JsonCallback callback) {
//    http://oa.dev.chuyuntech.com/oa/api/org/usersJson
        url = API + "org/usersJson";
        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(url)
                .addParams("org_ids", orgId)
                .build()
                .execute(callback);
    }


    /**
     * 收文详情
     *
     * @param tag
     * @param documentId
     * @param callback
     */
    public void receiptDetail(Object tag, int type, String documentId, JsonCallback callback) {
        //    http://oa.dev.chuyuntech.com/oa/api/receipt/detail

//        http://oa.dev.chuyuntech.com/oa/api/niwen/detail  发文详情
        if (type == 1) {
            url = API + "receipt/detail";
        } else {
            url = API + "niwen/detail";
        }

        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(url)
                .addParams("document_id", documentId)
                .build()
                .execute(callback);
    }

    /**
     * 下载文件
     *
     * @param tag
     * @param attachId
     * @param callback
     */
    public void download(Object tag, String attachId, FileCallBack callback) {
        //    http://oa.dev.chuyuntech.com/oa/api/document/download
        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(API + "document/download")
                .addParams("attach_id", attachId)
                .build()
                .execute(callback);
    }


    /**
     * 公文流转详情
     *
     * @param tag
     * @param docId
     * @param callback
     */
    public void flowDetail(Object tag, String docId, JsonCallback callback) {
        //    http://132.232.4.78/api/receipt/lzxq
        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(API + "receipt/lzxq")
                .addParams("document_id", docId)
                .build()
                .execute(callback);
    }


    //    收文主流程办理查找下一个主流程的处理人
    public void findNextMainFlowUsers(Object tag, String handleId, JsonCallback callback) {
        //    http://oa.dev.chuyuntech.com/oa/api/receipt/findNextMainFlowUsers
        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(API + "receipt/findNextMainFlowUsers")
                .addParams("handle_id", handleId)
                .build()
                .execute(callback);
    }

    //    http://oa.dev.chuyuntech.com/oa/api/receipt/detail    收文保存意见
    public void saveOpinion(Object tag, String handleId, String toUserId, String opinion, boolean isSaveCommonWords, JsonCallback callback) {

        Map<String, String> params = new HashMap<>();
        if (LEmptyTool.isNotEmpty(toUserId)) {
            params.put("user_id", toUserId);
        }
        params.put("handle_id", handleId);
        params.put("opinion", opinion);
        if (isSaveCommonWords)
            params.put("isSaveCommonWords", "1");
        else
            params.put("isSaveCommonWords", "0");

        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(API + "receipt/saveOpinion")
                .params(params)
                .build()
                .execute(callback);
    }

    /**
     * 文件瞎子
     *
     * @param tag
     * @param attachId
     * @param callback
     */
    public void downloadPdf(Object tag, String attachId, FileCallBack callback) {
        //    http://oa.dev.chuyuntech.com/oa/api/document/downloadPdf
        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(API + "document/downloadPdf")
                .addParams("attach_id", attachId)
                .build()
                .execute(callback);
    }


    //文件上传
    public void uploadWord(Object tag, String attachId, File file, JsonCallback callback) {
        //    http://oa.dev.chuyuntech.com/oa/api/document/uploadWord

        OkHttpUtils
                .post()
                .addHeader("username", username)
                .addHeader("password", password)
                .addHeader("Content-Type", "multipart/form-data")
                .addFile("file", file.getName(), file)
                .tag(tag)
                .url(API + "document/uploadWord?attach_id=" + attachId)
                .build()
                .execute(callback);
    }


    //    http://oa.dev.chuyuntech.com/oa/api/document/todoListParamList   待办公文查询条件列表
    public void todoListParamList(Object tag, JsonCallback callback) {
        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(API + "document/todoListParamList")
                .build()
                .execute(callback);
    }


    /**
     * 我的收藏分组
     *
     * @param tag
     * @param callback
     */
    //    http://oa.dev.chuyuntech.com/oa/api/document/myGroupList
    public void myGroupList(Object tag, JsonCallback callback) {
        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(API + "document/myGroupList")
                .build()
                .execute(callback);
    }


    /**
     * 收藏公文
     *
     * @param tag
     * @param callback
     */
    public void addCollect(Object tag, String groupId, String documentId, JsonCallback callback) {
        //    http://oa.dev.chuyuntech.com/oa/api/document/addCollect
        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(API + "document/addCollect")
                .addParams("collect_group_id", groupId)
                .addParams("document_id", documentId)
                .build()
                .execute(callback);
    }

    // 我的收藏列表分页
    public void collectListJson(Object tag, String groupId, JsonCallback callback) {
//        http://oa.dev.chuyuntech.com/oa/api/document/collectListJson
        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(API + "document/collectListJson")
                .addParams("collect_group_id", groupId)
                .build()
                .execute(callback);
    }

    //    http://oa.dev.chuyuntech.com/oa/api/document/followGroupJson   我的关注人员列表
    public void followGroupJson(Object tag, JsonCallback callback) {
        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(API + "document/followGroupJson")
                .build()
                .execute(callback);
    }

    //    http://oa.dev.chuyuntech.com/oa/api/document/followDocumentJson   我的关注公文查询
    public void followDocumentJson(Object tag, String followUserId, int page, int size, JsonCallback callback) {


        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(API + "document/followDocumentJson")
                .addParams("follow_user_id", followUserId)
                .addParams("page", page + "")
                .addParams("rows", size + "")
                .build()
                .execute(callback);
    }


    //    http://oa.dev.chuyuntech.com/oa/api/document/searchJson
    public void searchJson(Object tag, String docType, int page, int size, String title
            , String gwz, String qh, String unit, String begTime, String endTime, JsonCallback callback) {
      /*  group_id
        0 收文 1 发文
                rows 20
        page 1
        title 测试
        gwz 公文字
        qh 期号
        unit 单位
        begTime 开始时间   格式：2018-12-11
        endTime 结束时间   格式：2018-12-11
*/

        Map<String, String> params = new HashMap<>();

        params.put("page", page + "");
        params.put("rows", size + "");
        if (docType != null) {
            params.put("group_id", docType);
        }
        if (title != null) {
            params.put("title", title);
        }
        if (gwz != null) {
            params.put("gwz", gwz);
        }
        if (qh != null) {
            params.put("qh", qh);
        }
        if (unit != null) {
            params.put("unit", unit);
        }
        if (begTime != null) {
            params.put("begTime", begTime);
        }
        if (endTime != null) {
            params.put("endTime", endTime);
        }
        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(API + "document/searchJson")
                .params(params)
                .build()
                .execute(callback);
    }


    //    http://oa.dev.chuyuntech.com/oa/api/receipt/saveJobs  收文流程定义
    public void saveJobs(Object tag, String document_id, String handle_id, String user_id, String child_flow_id, JsonCallback callback) {

       /* document_id
                公文ID
        handle_id
                流程ID
        user_id
                多个用户ID
        child_flow_id
                多个流程ID
        isNeedReturn*/

        OkHttpUtils
                .get()
                .addHeader("username", username)
                .addHeader("password", password)
                .tag(tag)
                .url(API + "receipt/saveJobs")
                .addParams("document_id", document_id)
                .addParams("handle_id", handle_id)
                .addParams("user_id", user_id)
                .addParams("child_flow_id", child_flow_id)
                .addParams("isNeedReturn", "1")
                .build()
                .execute(callback);
    }
}
