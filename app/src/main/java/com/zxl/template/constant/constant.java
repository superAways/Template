package com.zxl.template.constant;

import android.os.Environment;

/**
 * author : 王新海
 * time   : 2018/11/28.
 * version: 1.0.0
 * desc   :
 */
public interface constant {

    String TYPE = "type";
    String DATA = "data";


    String PAGE_DOC = "DOC";
    String PAGE_QUERY = "QUERY";
    String PAGE_COLLECT = "COLLECT";


    String BROADCAST_VPN_SERVICE = "com.zxl.template.vpn";


    String SERVICE_IWEBOFFICE = "com.kinggrid.iappofficeapi";
    String BROADCAST_BACK_DOWN = "com.kinggrid.iappoffice.back";
    String BROADCAST_HOME_DOWN = "com.kinggrid.iappoffice.home";
    String BROADCAST_FILE_SAVE = "com.kinggrid.iappoffice.save";
    String BROADCAST_FILE_CLOSE = "com.kinggrid.iappoffice.close";
    String BROADCAST_FILE_SAVE_PIC = "com.kinggrid.iappoffice.save.pic";
    String BROADCAST_FILE_SAVEAS_PDF = "com.kinggrid.file.saveas.end";
    String BROADCAST_VIS_IMG = "cn.kg.broadcast.visibleimg";
    String BROADCAST_IMG_PATH = "com.kinggrid.imgpath";
    String BROADCAST_SIGN_IMG_PATH = "com.kinggrid.signimgpath";
    String BROADCAST_CLOSE_WPS = "return_close_wps";
    String SERVICE_IAPPOFFICE = "com.kinggrid.iappofficeservice";
    String BROADCAST_NOT_FINDOFF = "com.kinggrid.notfindoffice";
    String BROADCAST_SIGN_CHECKBOX_CHANGED = "com.kinggrid.signature.checkbox.changed";
    String BROADCAST_SIGNATURE_SHOW = "com.kinggrid.iappoffice.fullsignature.show";
    String BROADCAST_FILE_OPEN_END = "com.kinggrid.file.open.end";
    String BROADCAST_FILE_SAVEAS = "com.kinggrid.iappoffice.saveas";


    int DOC_TYPE_RECEIPT = 1;//1 收文 2发文
    int DOC_TYPE_DISPATCH = 2;


    int TEXT_FILE = 1;  //1 正文  2 附件

    String FILE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/MobileDoc/";

    String COPY_RIGHT = "SxD/phFsuhBWZSmMVtSjKZmm/c/3zSMrkV2Bbj5tznSkEVZmTwJv0wwMmH/+p6wLiUHbjadYueX9v51H9GgnjUhmNW1xPkB++KQqSv/VKLDsR8V6RvNmv0xyTLOrQoGzAT81iKFYb1SZ/Zera1cjGwQSq79AcI/N/6DgBIfpnlwiEiP2am/4w4+38lfUELaNFry8HbpbpTqV4sqXN1WpeJ7CHHwcDBnMVj8djMthFaapMFm/i6swvGEQ2JoygFU38558QhLaX/Jr1koWwK15kEmTvMG/nhvTilibTVNrCtS1VzVwUlmsSQF90/OYTdIhuI7ZTJqU764j1aPQnpiJKCoW/T3lHnHSqUG9ekm19Ty6lZhbc0wZsYY7F8fGE4DHZs4YOxNd7fnxKM4Dd4Klf1+OBmeDcECRDSxKMJbBZX2hOjA9xBaBPEEybxot0XBztGIYfNWOY83ltjqKjeToDSIzRqZYYUbAtzuYjy8C+3Ix04ev8Hm/as8N+FyWSdRWDRk0QFLuqgyKytzjbVWa9W3+KjjXk4eWYscdbEPDzss=";


    //选择机构
    String CONTACT_ATY_TYPE = "contact_aty";
    String CONTACT_FRG_type = "contact_frg";


    String EVENTBUS_AGENCY_DOC_REGRESH_LIST = "agency_doc";
    String EVENTBUS_AGENCY_DOC_REGRESH_ALL = "agency_doc_all";

    String EVENTBUS_DO_DOC_REGRESH_LIST = "do_doc";
    String EVENTBUS_DO_DOC_REGRESH_ALL = "do_doc_all";
}
