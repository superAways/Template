package com.zxl.template.view.pop;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yhy.widget.core.toggle.SwitchButton;
import com.zxl.template.R;
import com.zxl.template.http.ApiControl;
import com.zxl.template.http.JsonCallback;
import com.zxl.template.model.DocParams;
import com.zxl.zlibrary.tool.LEmptyTool;
import com.zxl.zlibrary.tool.LToast;
import com.zxl.zlibrary.tool.LogTool;

import java.util.List;
import java.util.PropertyResourceBundle;

import okhttp3.Call;
import razerdp.basepopup.BasePopupWindow;

/*
 * @created by LoveLing on 2018/11/23
 * @emil 1079112877@qq.com
 * description:
 */
public class DocParamsPop extends BasePopupWindow {
    private static final String TAG = "DocParamsPop";

    private Context mContext;
    private OnItemSelectOnClickListener mListener;
    private RecyclerView recyclerView;
    List<DocParams> api;
    private SwitchButton switchButton;
    private Boolean isChecked;
    private OnAllOnClick onAllOnClick;
    private RelativeLayout rlAll;
    private int type;

    public DocParamsPop(Context context, List<DocParams> api, int type) {
        super(context);
        this.api = api;
        this.mContext = context;
        this.type = type;
        initView();
    }

    private void initView() {

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        RvAdapter rvAdapter = new RvAdapter(api);
        recyclerView.setAdapter(rvAdapter);

        switchButton = findViewById(R.id.btn_switch);
        rlAll = findViewById(R.id.rl_doc_all);

        if (type == 2)
            findViewById(R.id.rl_doc_type).setVisibility(View.GONE);


        rvAdapter.setOnItemClickListener((adapter, view, position) -> {
            if (LEmptyTool.isNotEmpty(mListener)) {
                DocParams item = (DocParams) adapter.getItem(position);
                assert item != null;
                if (type == 1)
                    item.isSwitch = isChecked;
                mListener.selected(item);
            }
        });

        switchButton.setOnStateChangeListener((view, isChecked) -> {
            this.isChecked = isChecked;
        });


        rlAll.setOnClickListener(v -> {
            onAllOnClick.all();
        });
    }


    @Override
    protected Animation onCreateDismissAnimation() {
        return null;
    }


    @Override
    public View onCreateContentView() {
        return createPopupById(R.layout.pop_doc_params);
    }

    public void SetOnItemSelectOnClickListener(OnItemSelectOnClickListener clickListener) {
        this.mListener = clickListener;
    }


    public interface OnItemSelectOnClickListener {
        void selected(DocParams docParams);
    }

    public interface OnAllOnClick {
        void all();
    }

    public void setOnAllOnClick(OnAllOnClick onAllOnClick) {
        this.onAllOnClick = onAllOnClick;
    }

    class RvAdapter extends BaseQuickAdapter<DocParams, BaseViewHolder> {

        public RvAdapter(@Nullable List<DocParams> data) {
            super(R.layout.item_doc_parmas, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, DocParams item) {

            helper.setText(R.id.tv_name, item.flowName);

            if (type == 1) {
                helper.setText(R.id.tv_nums, item.nums + "");

            } else if (type == 2) {
                helper.getView(R.id.tv_nums).setVisibility(View.GONE);
            }
        }
    }


}
