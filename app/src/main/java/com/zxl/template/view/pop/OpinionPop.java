package com.zxl.template.view.pop;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zxl.template.R;
import com.zxl.template.model.Opinion;

import java.util.ArrayList;
import java.util.List;

import razerdp.basepopup.BasePopupWindow;

/*
 * @created by LoveLing on 2018/11/23
 * @emil 1079112877@qq.com
 * description: 意见POP
 */
public class OpinionPop extends BasePopupWindow {

    private Context mContext;
    private RecyclerView recyclerView;
    private List<Opinion> opinions;

    private OnCheckListener mListener;

    public OpinionPop(Context context, List<Opinion> opinions) {
        super(context);
        this.mContext = context;
        this.opinions = opinions;
        initView();
    }

    private void initView() {
        findViewById(R.id.tv_close).setOnClickListener(v -> this.dismiss());
        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        RvAdapter rvAdapter = new RvAdapter(opinions);
        recyclerView.setAdapter(rvAdapter);

        rvAdapter.setOnItemClickListener((adapter, view, position) -> {
            Opinion item = (Opinion) adapter.getItem(position);
            assert item != null;
            mListener.checked(item.words);
        });

    }

    public void setOnCheckListener(OnCheckListener listener) {
        this.mListener = listener;
    }

    public interface OnCheckListener {
        void checked(String item);
    }


    @Override
    protected Animation onCreateDismissAnimation() {
        return null;
    }


    @Override
    public View onCreateContentView() {
        return createPopupById(R.layout.pop_opinion);
    }


    class RvAdapter extends BaseQuickAdapter<Opinion, BaseViewHolder> {

        public RvAdapter(@Nullable List<Opinion> data) {
            super(R.layout.item_opinion, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, Opinion item) {
            helper.setText(R.id.tv_opinion, item.words);
        }
    }

}
