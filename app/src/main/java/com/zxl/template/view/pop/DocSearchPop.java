package com.zxl.template.view.pop;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yhy.widget.core.toggle.SwitchButton;
import com.zxl.template.R;
import com.zxl.zlibrary.tool.LToast;

import java.util.Calendar;

import cn.qqtheme.framework.picker.DatePicker;
import cn.qqtheme.framework.util.ConvertUtils;


/**
 * author : 颜洪毅
 * e-mail : yhyzgn@gmail.com
 * time   : 2018-03-16 13:33
 * version: 1.0.0
 * desc   :
 */
public class DocSearchPop extends BasePopup {
    private View mAnchor;
    private View mView;
    private TextView tvMsg;
    private TextView tvSearch;
    private TextView tvShare;
    private TextView tvPhone;
    private OnQueryClickListener mListener;
    private final ImageView ivClose;
    private final SwitchButton sbDoc;
    private final EditText etTitle;
    private final EditText etContent;
    private final EditText etYearNumber;
    private final EditText etIssueNumber;
    private final TextView tvStartDate;
    private final TextView tvEndDate;
    private final TextView tvReset;
    private final TextView tvQuery;

    Calendar calendar = Calendar.getInstance();
    //获取系统的日期
    //年
    private int year = calendar.get(Calendar.YEAR);
    //月
    private int month = calendar.get(Calendar.MONTH) + 1;
    //日
    private int day = calendar.get(Calendar.DAY_OF_MONTH);

    private int startYear, startMonth, startDay;
    private int endYear, endMonth, endDay;
    private final RelativeLayout rlAll;

    private OnAllListener onAllListener;

    public DocSearchPop(Activity activity, View anchor) {
        super(activity, anchor.getRootView());

        mAnchor = anchor;

        mView = LayoutInflater.from(activity).inflate(R.layout.pop_doc_search, null);
        ivClose = mView.findViewById(R.id.iv_close);
        sbDoc = mView.findViewById(R.id.sb_doc_type);
        etTitle = mView.findViewById(R.id.et_title);
        etContent = mView.findViewById(R.id.et_content);
        etYearNumber = mView.findViewById(R.id.et_year_number);
        etIssueNumber = mView.findViewById(R.id.et_issue_number);
        tvStartDate = mView.findViewById(R.id.tv_start_date);
        tvEndDate = mView.findViewById(R.id.tv_end_date);
        tvReset = mView.findViewById(R.id.tv_reset);
        tvQuery = mView.findViewById(R.id.tv_query);

        rlAll = mView.findViewById(R.id.rl_all);

        mView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

        initListener();

        setContentView(mView);
        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        setFocusable(true);
        setAnimationStyle(R.style.TopPopAnimation);
    }

    private void initListener() {
        ivClose.setOnClickListener(v -> {
            dismiss();
        });

        tvReset.setOnClickListener(v -> {
            sbDoc.onOrOff(false);
            etTitle.setText("");
            etContent.setText("");
            etYearNumber.setText("");
            etIssueNumber.setText("");
            etTitle.setHint("请输入标题关键字");
            etContent.setHint("请输入公文字");
            etYearNumber.setHint("请输入年号");
            etIssueNumber.setHint("请输入期号");
            tvStartDate.setText("");
            tvStartDate.setHint("请选择开始日期");
            tvEndDate.setText("");
            tvEndDate.setHint("请选择结束日期");

        });


        tvStartDate.setOnClickListener(v -> {
            //时间选择器
            DatePicker picker = new DatePicker(mActivity);
            picker.setTopLineColor(mActivity.getResources().getColor(R.color.colorAccent));
            picker.setDividerColor(mActivity.getResources().getColor(R.color.colorAccent));
            picker.setTextColor(mActivity.getResources().getColor(R.color.colorAccent));
            picker.setLabelTextColor(mActivity.getResources().getColor(R.color.colorAccent));
            picker.setCancelTextColor(mActivity.getResources().getColor(R.color.colorAccent));
            picker.setSubmitTextColor(mActivity.getResources().getColor(R.color.colorAccent));
            picker.setCanceledOnTouchOutside(true);
            picker.setUseWeight(true);
            picker.setTopPadding(ConvertUtils.toPx(mActivity, 10));
            picker.setRangeStart(2000, 1, 1);
            if (endYear != 0 && endMonth != 0 && endDay != 0) {
                picker.setRangeEnd(endYear, endMonth, endDay);
            } else {
                picker.setRangeEnd(year, month, day);
            }
            picker.setResetWhileWheel(false);

            if (startYear != 0 && startMonth != 0 && startDay != 0) {
                picker.setSelectedItem(startYear, startMonth, startDay);
                picker.setTitleText(startYear + "-" + startMonth + "-" + startDay);
            } else {
                if (endYear != 0 && endMonth != 0 && endDay != 0) {
                    picker.setSelectedItem(endYear, endMonth, endDay);
                } else {
                    picker.setSelectedItem(year, month, day);
                }
            }

            picker.setOnDatePickListener((DatePicker.OnYearMonthDayPickListener) (y, m, d) -> {
                LToast.normal(y + "-" + m + "-" + d);
                startYear = Integer.valueOf(y);
                startMonth = Integer.valueOf(m);
                startDay = Integer.valueOf(d);
                tvStartDate.setText(startYear + "-" + startMonth + "-" + startDay);
            });
            picker.setOnWheelListener(new DatePicker.OnWheelListener() {
                @Override
                public void onYearWheeled(int index, String year) {
                    picker.setTitleText(year + "-" + picker.getSelectedMonth() + "-" + picker.getSelectedDay());
                }

                @Override
                public void onMonthWheeled(int index, String month) {
                    picker.setTitleText(picker.getSelectedYear() + "-" + month + "-" + picker.getSelectedDay());
                }

                @Override
                public void onDayWheeled(int index, String day) {
                    picker.setTitleText(picker.getSelectedYear() + "-" + picker.getSelectedMonth() + "-" + day);
                }
            });
            picker.show();
        });

        rlAll.setOnClickListener(v -> onAllListener.all());

        //报名截止日期
        tvEndDate.setOnClickListener((View v) -> {
            //时间选择器
            DatePicker picker = new DatePicker(mActivity);
            picker.setTopLineColor(mActivity.getResources().getColor(R.color.colorAccent));
            picker.setDividerColor(mActivity.getResources().getColor(R.color.colorAccent));
            picker.setTextColor(mActivity.getResources().getColor(R.color.colorAccent));
            picker.setLabelTextColor(mActivity.getResources().getColor(R.color.colorAccent));
            picker.setCancelTextColor(mActivity.getResources().getColor(R.color.colorAccent));
            picker.setSubmitTextColor(mActivity.getResources().getColor(R.color.colorAccent));
            picker.setCanceledOnTouchOutside(true);
            picker.setUseWeight(true);
            picker.setTopPadding(ConvertUtils.toPx(mActivity, 10));
            if (startYear != 0 && startMonth != 0 && startDay != 0) {
                picker.setRangeStart(startYear, startMonth, startDay);
            } else {
                picker.setRangeStart(2000, 1, 1);
            }
            picker.setRangeEnd(year, month, day);
            picker.setResetWhileWheel(false);

            if (endYear != 0 && endMonth != 0 && endDay != 0) {
                picker.setSelectedItem(endYear, endMonth, endDay);
                picker.setTitleText(endYear + "-" + endMonth + "-" + endDay);
            } else {
                if (startYear != 0 && startMonth != 0 && startDay != 0) {
                    picker.setSelectedItem(startYear, startMonth, startDay);
                } else {
                    picker.setSelectedItem(year, month, day);
                }
            }

            picker.setOnDatePickListener((DatePicker.OnYearMonthDayPickListener) (y, m, d) -> {
                LToast.normal(y + "-" + m + "-" + d);
                endYear = Integer.valueOf(y);
                endMonth = Integer.valueOf(m);
                endDay = Integer.valueOf(d);
                tvEndDate.setText(endYear + "-" + endMonth + "-" + endDay);
            });
            picker.setOnWheelListener(new DatePicker.OnWheelListener() {
                @Override
                public void onYearWheeled(int index, String year) {
                    picker.setTitleText(year + "-" + picker.getSelectedMonth() + "-" + picker.getSelectedDay());
                }

                @Override
                public void onMonthWheeled(int index, String month) {
                    picker.setTitleText(picker.getSelectedYear() + "-" + month + "-" + picker.getSelectedDay());
                }

                @Override
                public void onDayWheeled(int index, String day) {
                    picker.setTitleText(picker.getSelectedYear() + "-" + picker.getSelectedMonth() + "-" + day);
                }
            });
            picker.show();
        });


        tvQuery.setOnClickListener(v -> {
            boolean docType = sbDoc.isOn();
            String title = etTitle.getText().toString().trim();
            String content = etContent.getText().toString().trim();
            String yearNumber = etYearNumber.getText().toString().trim();
            String issueNumber = etIssueNumber.getText().toString().trim();
            String startDate = tvStartDate.getText().toString().trim();
            String endDate = tvEndDate.getText().toString().trim();

            if (mListener != null) {
                mListener.queryDoc(docType, title, content, yearNumber, issueNumber, startDate, endDate);
            }
        });

    }

    public void toggle() {
        toggle(true);
    }

    public void toggle(boolean bgAlpha) {
        if (!isShowing()) {
            showAsDropDown(mAnchor, 0, 0);
            if (bgAlpha) {
                backgroundAlpha(0.4f);
            }
        } else {
            dismiss();
        }
    }

    public void setOnQueryClickListener(OnQueryClickListener listener) {
        mListener = listener;
    }

    public interface OnQueryClickListener {
        //etYearNumber
        //etIssueNumber
        //tvStartDate =
        //tvEndDate = m
        void queryDoc(boolean docType, String title, String content, String yearNumber, String issueNumber, String startDate, String endDate);
    }

    public void setOnAllListener(OnAllListener onAllListener) {
        this.onAllListener = onAllListener;
    }

    public interface OnAllListener {
        void all();
    }


}
