package com.zxl.template.view.pop;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.PopupWindow;

import com.yhy.widget.core.dialog.LoadingDialog;


/**
 * author : 颜洪毅
 * e-mail : yhyzgn@gmail.com
 * time   : 2018-01-31 17:06
 * version: 1.0.0
 * desc   : 弹窗基类
 */
public class BasePopup extends PopupWindow {
    protected View mParent;
    protected Activity mActivity;
    protected LoadingDialog ldLoading;

    public BasePopup(Activity activity, View parent) {
        super(activity);
        mActivity = activity;
        mParent = parent;

        setWidth(LayoutParams.MATCH_PARENT);
        setHeight(LayoutParams.WRAP_CONTENT);

        setFocusable(true);
        setOutsideTouchable(true);
        // 刷新状态
        update();
        setBackgroundDrawable(new BitmapDrawable());

        ldLoading = new LoadingDialog(activity, "加载中...");
        ldLoading.setCancelable(true, false);
    }

    @Override
    public void dismiss() {
        backgroundAlpha(1f);
        super.dismiss();
    }

    public void backgroundAlpha(float bgAlpha) {
        LayoutParams lp = mActivity.getWindow().getAttributes();
        lp.alpha = bgAlpha; // 0.0-1.0
        mActivity.getWindow().setAttributes(lp);
    }
}
