package com.zxl.template.view.pop;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.zxl.template.R;


/**
 * author : 颜洪毅
 * e-mail : yhyzgn@gmail.com
 * time   : 2018-03-16 13:33
 * version: 1.0.0
 * desc   :
 */
public class DocPop extends BasePopup {
    private View mAnchor;
    private View mView;
    private TextView tvMsg;
    private TextView tvSearch;
    private TextView tvShare;
    private TextView tvPhone;
    private OnItemClickListener mListener;

    public DocPop(Activity activity, View anchor) {
        super(activity, anchor.getRootView());
        mAnchor = anchor;
        mView = LayoutInflater.from(activity).inflate(R.layout.pop_doc, null);

        mView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        initListener();

        setContentView(mView);
        setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        setFocusable(true);
        setAnimationStyle(R.style.TopPopAnimation);
    }

    private void initListener() {

    }

    public void toggle() {
        toggle(true);
    }

    public void toggle(boolean bgAlpha) {
        if (!isShowing()) {
            showAsDropDown(mAnchor,0, 0);
            if (bgAlpha) {
                backgroundAlpha(0.4f);
            }
        } else {
            dismiss();
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public interface OnItemClickListener {

    }
}
