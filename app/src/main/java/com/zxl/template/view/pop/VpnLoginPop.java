package com.zxl.template.view.pop;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zxl.template.App;
import com.zxl.template.R;
import com.zxl.template.activity.ContactAty;
import com.zxl.template.http.JsonCallback;
import com.zxl.template.model.ContactRows;
import com.zxl.template.model.DocRows;
import com.zxl.template.model.MsgEvent;
import com.zxl.template.model.Receipt;
import com.zxl.zlibrary.tool.LActivityTool;
import com.zxl.zlibrary.tool.LCacheTool;
import com.zxl.zlibrary.tool.LEmptyTool;
import com.zxl.zlibrary.tool.LToast;
import com.zxl.zlibrary.tool.LogTool;
import com.zxl.zlibrary.view.LClearEditText;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import mehdi.sakout.fancybuttons.FancyButton;
import okhttp3.Call;
import razerdp.basepopup.BasePopupWindow;

/*
 * @created by LoveLing on 2018/11/23
 * @emil 1079112877@qq.com
 * description: vpn登录
 */
public class VpnLoginPop extends BasePopupWindow {

    private Context mContext;
    private View btnCancel;
    private View btnSave;
    private BtnOnClickListener btnOnClickListener;
    private LClearEditText etAccount;
    private LClearEditText etPass;
    private final static String vpnPath = "/data/data/com.zxl.template/vpn";

    public VpnLoginPop(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }


    private void initView() {
        btnCancel = findViewById(R.id.btn_cancel);
        btnSave = findViewById(R.id.btn_save);
        etAccount = findViewById(R.id.et_account);
        etPass = findViewById(R.id.et_pass);

        LCacheTool cacheTool = LCacheTool.getInstance(new File(vpnPath));
        if (LEmptyTool.isNotEmpty(cacheTool)) {
            String account1 = cacheTool.getString("account");
            String password = cacheTool.getString("password");

            if (LEmptyTool.isNotEmpty(account1)) {
                etAccount.setText(account1);
            }

            if (LEmptyTool.isNotEmpty(password)) {
                etPass.setText(password);
            }
        }

        btnSave.setOnClickListener(v -> {
            String account = Objects.requireNonNull(etAccount.getText()).toString();
            String pass = Objects.requireNonNull(etPass.getText()).toString();

            if (LEmptyTool.isEmpty(account)) {
                LToast.normal("請輸入賬號");
                etAccount.setShakeAnimation();
                return;
            }
            if (LEmptyTool.isEmpty(account)) {
                LToast.normal("請輸入密碼");
                etPass.setShakeAnimation();
                return;
            }


            cacheTool.put("account", account);
            cacheTool.put("password", pass);

            btnOnClickListener.click(2);
            dismiss();
        });

        btnCancel.setOnClickListener(v -> {
            btnOnClickListener.click(1);
            dismiss();
        });
    }

    //1是取消  2是确定
    public interface BtnOnClickListener {
        void click(int i);
    }

    public void setBtnOnClickListener(BtnOnClickListener btnOnClickListener) {
        this.btnOnClickListener = btnOnClickListener;
    }


    @Override
    protected Animation onCreateDismissAnimation() {
        return null;
    }


    @Override
    public View onCreateContentView() {
        return createPopupById(R.layout.pop_vpn_login);
    }

}
