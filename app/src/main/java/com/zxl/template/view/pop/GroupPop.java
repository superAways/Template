package com.zxl.template.view.pop;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zxl.template.R;
import com.zxl.template.model.Group;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import razerdp.basepopup.BasePopupWindow;

/*
 * @created by LoveLing on 2018/11/23
 * @emil 1079112877@qq.com
 * description: 流程POP
 */
public class GroupPop extends BasePopupWindow {

    private Context mContext;
    private RvAdapter rvAdapter;
    private RecyclerView recyclerView;
    private ImageView ivCollect;

    private List<Group> groups;
    private OnClickListener clickListener;

    public GroupPop(Context context, List<Group> groups) {
        super(context);
        this.mContext = context;
        this.groups = groups;
        initView();
    }


    @Override
    public void dismiss() {
        super.dismiss();
        if (EventBus.getDefault().isRegistered(mContext)) {
            EventBus.getDefault().unregister(mContext);
        }

    }

    private void initView() {
        recyclerView = findViewById(R.id.recyclerView);
        ivCollect = findViewById(R.id.iv_close);

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        rvAdapter = new RvAdapter(groups);
        recyclerView.setAdapter(rvAdapter);

        rvAdapter.setOnItemClickListener((adapter, view, position) -> {
            Group item = (Group) adapter.getItem(position);
            clickListener.onClick(item);
        });

        ivCollect.setOnClickListener(v -> this.dismiss());
    }


    @Override
    protected Animation onCreateDismissAnimation() {
        return null;
    }


    @Override
    public View onCreateContentView() {
        return createPopupById(R.layout.pop_group);
    }

    public void setOnClickListener(OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface OnClickListener {
        void onClick(Group item);
    }


    class RvAdapter extends BaseQuickAdapter<Group, BaseViewHolder> {

        RvAdapter(List<Group> groups) {
            super(R.layout.item_pop_group, groups);
        }

        @Override
        protected void convert(BaseViewHolder helper, Group item) {
            helper.setText(R.id.tv_name, item.groupName);

        }
    }
}
