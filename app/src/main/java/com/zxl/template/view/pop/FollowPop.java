package com.zxl.template.view.pop;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.CycleInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zxl.template.R;
import com.zxl.template.model.FollowGroupUser;

import java.util.List;

import razerdp.basepopup.BasePopupWindow;

/*
 * @created by LoveLing on 2018/11/23
 * @emil 1079112877@qq.com
 * description:
 */
public class FollowPop extends BasePopupWindow {

    private Context mContext;
    private OnItemSelectOnClickListener mListener;
    private RecyclerView recyclerView;
    List<FollowGroupUser> followGroupUsers;

    public FollowPop(Context context, List<FollowGroupUser> followGroupUsers) {
        super(context);
        this.followGroupUsers = followGroupUsers;
        this.mContext = context;
        initView();
    }

    private void initView() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        RvAdapter rvAdapter = new RvAdapter(followGroupUsers);
        recyclerView.setAdapter(rvAdapter);

        rvAdapter.setOnItemClickListener((adapter, view, position) -> {
            FollowGroupUser item = (FollowGroupUser) adapter.getItem(position);
            assert item != null;
            mListener.click(item);
            dismiss();
        });

    }


    @Override
    protected Animation onCreateDismissAnimation() {
        return null;
    }


    @Override
    public View onCreateContentView() {
        return createPopupById(R.layout.pop_follow);
    }

    public void SetOnItemSelectOnClickListener(OnItemSelectOnClickListener clickListener) {
        this.mListener = clickListener;
    }

    public interface OnItemSelectOnClickListener {
        void click(FollowGroupUser followGroupUser);
    }

    class RvAdapter extends BaseQuickAdapter<FollowGroupUser, BaseViewHolder> {

        public RvAdapter(@Nullable List<FollowGroupUser> data) {
            super(R.layout.item_follow_group, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, FollowGroupUser item) {
            helper.setText(R.id.tv_name, item.text);
        }
    }

}
