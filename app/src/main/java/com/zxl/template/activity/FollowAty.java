package com.zxl.template.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.zxl.template.R;
import com.zxl.template.base.BaseAty;
import com.zxl.template.base.IActivity;
import com.zxl.template.http.JsonCallback;
import com.zxl.template.model.CollectRow;
import com.zxl.template.model.DocRows;
import com.zxl.template.model.Follow;
import com.zxl.template.model.FollowGroupUser;
import com.zxl.template.model.FollowRow;
import com.zxl.template.model.Test;
import com.zxl.template.view.pop.FollowPop;
import com.zxl.zlibrary.tool.LActivityTool;
import com.zxl.zlibrary.tool.LCacheTool;
import com.zxl.zlibrary.tool.LEmptyTool;
import com.zxl.zlibrary.tool.LToast;
import com.zxl.zlibrary.tool.LogTool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/*
 * @created by LoveLing on 2018/11/23
 * @emil 1079112877@qq.com
 * description:我的关注
 */
public class FollowAty extends BaseAty implements IActivity {

    private static final String TAG = "FollowAty";

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.iv_screen)
    ImageView ivScreen;
    @BindView(R.id.titleBar)
    RelativeLayout titleBar;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    private List<FollowGroupUser> followGroupUserList = new ArrayList<>();
    private String followUserId;
    private int page = 1;
    private int size = 50;
    private List<FollowRow> followRows = new ArrayList<>();
    private RvAdapter rvAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aty_follow);
        ButterKnife.bind(this);
        initView();
        initData();
        initListener();
    }

    @Override
    public void initListener() {
        refreshLayout.setOnRefreshListener(refreshLayout -> {
            refreshLayout.finishRefresh(2000/*,false*/);//传入false表示刷新失败
        });
        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            refreshLayout.finishLoadMore(2000/*,false*/);//传入false表示加载失败
        });
    }

    @Override
    public void initData() {
        mApi.followGroupJson(this, new JsonCallback<List<FollowGroupUser>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogTool.e(TAG, e);
            }

            @Override
            public void onResponse(List<FollowGroupUser> followGroupUsers, int i) {
                if (LEmptyTool.isNotEmpty(followGroupUsers)) {
                    followGroupUserList.clear();
                    followGroupUserList.addAll(followGroupUsers);
                    followUserId = followGroupUsers.get(0).id;
                    initNet();
                }
            }
        });
    }

    private void initNet() {
        mApi.followDocumentJson(this, followUserId, page, size, new JsonCallback<Follow>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogTool.e(TAG, e);
            }

            @Override
            public void onResponse(Follow follow, int i) {
                followRows.clear();
                followRows.addAll(follow.followRows);
                rvAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void initView() {

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        rvAdapter = new RvAdapter(followRows);
        recyclerView.setAdapter(rvAdapter);

        rvAdapter.setOnItemClickListener((adapter, view, position) -> {
            FollowRow item = (FollowRow) adapter.getItem(position);

            DocRows docRows = new DocRows();
            assert item != null;
            docRows.documentId = item.documentId;
            docRows.documentType = item.documentType;

            Bundle args = new Bundle();
            args.putSerializable("data", docRows);
            LActivityTool.startActivity(args, DocDetailActivity.class);
        });
    }


    @OnClick({R.id.iv_back, R.id.iv_screen})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_screen:
                if (LEmptyTool.isNotEmpty(followGroupUserList)) {
                    FollowPop followPop = new FollowPop(FollowAty.this, followGroupUserList);
                    followPop.showPopupWindow(titleBar);
                    followPop.SetOnItemSelectOnClickListener(item -> {
                        followUserId = item.id;
                        initNet();
                    });
                }
                break;
        }
    }

    class RvAdapter extends BaseQuickAdapter<FollowRow, BaseViewHolder> {

        RvAdapter(@Nullable List<FollowRow> data) {
            super(R.layout.item_follow, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, FollowRow item) {
            helper.setText(R.id.tv_title, item.docTitle);
            helper.setText(R.id.tv_sub_title, item.sender + "，" + item.opinion);
            helper.setText(R.id.tv_time, item.createTime);
            helper.setText(R.id.tv_status, item.title);
        }
    }
}
