package com.zxl.template.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.alibaba.fastjson.JSON;
import com.zxl.template.R;
import com.zxl.template.base.BaseAty;
import com.zxl.template.base.IActivity;
import com.zxl.template.constant.constant;
import com.zxl.template.http.JsonCallback;
import com.zxl.template.model.Dept;
import com.zxl.template.model.MsgEvent;
import com.zxl.template.view.tree.TreeViewFactory;
import com.zxl.zlibrary.tool.LogTool;
import com.zxl.zlibrary.view.LTitleBarView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.texy.treeview.TreeNode;
import me.texy.treeview.TreeView;
import okhttp3.Call;

/*
 * @created by LoveLing on 2018/11/23
 * @emil 1079112877@qq.com
 * description:
 */
public class DeptAty extends BaseAty implements IActivity, constant {
    private static final String TAG = "DeptAty";


    private TreeNode root;
    private TreeView treeView;


    @BindView(R.id.titleBar)
    LTitleBarView titleBar;
    @BindView(R.id.container)
    FrameLayout container;

    private List<Dept> deptList = new ArrayList<>();
    private String type;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aty_dept);
        ButterKnife.bind(this);
        initView();
        initData();
        initListener();
    }

    @Override
    public void initView() {

    }

    @Override
    public void initListener() {
        type = getIntent().getStringExtra(TYPE);
        titleBar.setLeftOnClickListener(v -> finish());
        titleBar.setRightTextOnClickListener(v -> {
            List<Dept> list = getSelectedNodes();

            if (type != null && type.equals(CONTACT_ATY_TYPE)) {
                EventBus.getDefault().post(new MsgEvent<>(CONTACT_ATY_TYPE, list));
            } else {
                EventBus.getDefault().post(new MsgEvent<>(CONTACT_FRG_type, list));
            }
            finish();
        });
    }


    @Override
    public void initData() {
        mApi.deptList(this, new JsonCallback<List<Dept>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogTool.e(TAG, e);
            }

            @Override
            public void onResponse(List<Dept> depts, int i) {
                deptList.clear();
                deptList.addAll(depts);


                //生成树
                root = TreeNode.root();
                buildTree();
                treeView = new TreeView(root, DeptAty.this, new TreeViewFactory());
                View view = treeView.getView();
                view.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                container.addView(view);


            }
        });
    }

    private void buildTree() {

        List<Dept> pDeptList = new ArrayList<>();

        //弄一科树木
        for (Dept dept : deptList) {
            //第一级别
            if (dept.fid.equals("0")) {
                List<Dept> subList = new ArrayList<>();
                for (Dept subDept : deptList) {
                    if (subDept.fid.equals(dept.id)) {
                        subList.add(subDept);
                    }
                }
                dept.subDept = subList;
                pDeptList.add(dept);
            }
        }


        LogTool.d(JSON.toJSONString(pDeptList));


        for (int i = 0; i < pDeptList.size(); i++) {
            TreeNode treeNode = new TreeNode(pDeptList.get(i));
            treeNode.setLevel(0);
            treeNode.setExpanded(true);
            for (int j = 0; j < pDeptList.get(i).subDept.size(); j++) {
                TreeNode treeNode1 = new TreeNode(pDeptList.get(i).subDept.get(j));
                treeNode1.setLevel(1);
                treeNode.addChild(treeNode1);
            }

            root.addChild(treeNode);
        }
    }

    private List<Dept> getSelectedNodes() {
        List<Dept> depts = new ArrayList<>();
        List<TreeNode> selectedNodes = treeView.getSelectedNodes();
        for (int i = 0; i < selectedNodes.size(); i++) {
            Dept dept = (Dept) selectedNodes.get(i).getValue();
            depts.add(dept);
        }
        return depts;
    }


}
