package com.zxl.template.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zxl.template.R;
import com.zxl.template.base.BaseAty;
import com.zxl.template.base.IActivity;
import com.zxl.template.constant.constant;
import com.zxl.template.http.JsonCallback;
import com.zxl.template.model.Contact;
import com.zxl.template.model.ContactRows;
import com.zxl.template.model.Dept;
import com.zxl.template.model.MsgEvent;
import com.zxl.zlibrary.tool.LActivityTool;
import com.zxl.zlibrary.tool.LEmptyTool;
import com.zxl.zlibrary.tool.LToast;
import com.zxl.zlibrary.tool.LogTool;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/*
 * @created by LoveLing on 2018/11/23
 * @emil 1079112877@qq.com
 * description:
 */
public class ContactAty extends BaseAty implements IActivity, constant {
    private static final String TAG = "ContactAty";
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private String orgId;

    private List<ContactRows> contactRows = new ArrayList<>();
    private RvAdapter rvAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aty_operator_selected);
        ButterKnife.bind(this);
        initView();
        initData();
        initListener();

        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void initView() {
        if (LEmptyTool.isNotEmpty(mApi.mCurrentUser))
            orgId = mApi.mCurrentUser.orgId;


        recyclerView.setLayoutManager(new LinearLayoutManager(ContactAty.this));
        rvAdapter = new RvAdapter(contactRows);
        recyclerView.setAdapter(rvAdapter);
    }


    @Override
    public void initListener() {

    }


    @Override
    public void initData() {
        mApi.contactList(this, orgId, new JsonCallback<Contact>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogTool.e(TAG, e);
            }

            @Override
            public void onResponse(Contact contact, int i) {
                contactRows.clear();
                contactRows.addAll(contact.rows);
                rvAdapter.notifyDataSetChanged();
            }
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MsgEvent<List<Dept>> event) {
        if (event.type.equals(CONTACT_ATY_TYPE)) {
            StringBuilder sb = new StringBuilder();
            for (Dept dept : event.t) {
                sb.append(",");
                sb.append(dept.id);
            }
            if (sb.length() > 0)
                sb.deleteCharAt(0);
            orgId = sb.toString();
            initData();
        }
    }

    @OnClick({R.id.iv_back, R.id.tv_screen, R.id.btn_confirm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_screen:
                Bundle args = new Bundle();
                args.putString(TYPE, CONTACT_ATY_TYPE);
                LActivityTool.startActivity(args, DeptAty.class);
                break;
            case R.id.btn_confirm:

                Iterator<ContactRows> iterator = contactRows.iterator();
                while (iterator.hasNext()) {
                    ContactRows item = iterator.next();
                    if (!item.isCheck) {
                        iterator.remove();
                    }
                }

                if (contactRows.size() <= 0) {
                    LToast.warning("至少选择一个人.");
                    return;
                }

                EventBus.getDefault().post(new MsgEvent<>("contact_pop", contactRows));

                finish();

                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    private class RvAdapter extends BaseQuickAdapter<ContactRows, BaseViewHolder> {

        RvAdapter(@Nullable List<ContactRows> data) {
            super(R.layout.item_operator_rv, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, ContactRows item) {
            int position = helper.getPosition();

            helper.setText(R.id.tv_name, item.userCnName);
            helper.setText(R.id.tv_dept, item.orgName);

            ((AppCompatCheckBox) helper.getView(R.id.checkBox))
                    .setOnCheckedChangeListener((buttonView, isChecked) -> {
                        contactRows.get(position).isCheck = true;
                    });
        }
    }
}
