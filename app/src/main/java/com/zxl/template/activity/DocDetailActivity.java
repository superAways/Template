package com.zxl.template.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.flyco.tablayout.SlidingTabLayout;
import com.zxl.template.R;
import com.zxl.template.adapter.TbVPAdapter;
import com.zxl.template.base.BaseAty;
import com.zxl.template.base.BaseFrag;
import com.zxl.template.base.IActivity;
import com.zxl.template.constant.constant;
import com.zxl.template.fragment.DocDetailPager;
import com.zxl.template.fragment.FlowDocDetailPager;
import com.zxl.template.http.JsonCallback;
import com.zxl.template.model.DocParams;
import com.zxl.template.model.DocRows;
import com.zxl.template.model.Group;
import com.zxl.template.model.MsgEvent;
import com.zxl.template.view.pop.GroupPop;
import com.zxl.zlibrary.tool.LActivityTool;
import com.zxl.zlibrary.tool.LEmptyTool;
import com.zxl.zlibrary.tool.LToast;
import com.zxl.zlibrary.tool.LogTool;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * author : 王新海
 * time   : 2018/11/21.
 * version: 1.0.0
 * desc   :
 */
public class DocDetailActivity extends BaseAty implements IActivity, constant {
    private static final String TAG = "DocDetailActivity";

    @BindView(R.id.tabLayout)
    SlidingTabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager tabViewPager;
    private DocRows docRows;
    private List<BaseFrag> fragments;
    private List<String> titles;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doc_detail);
        ButterKnife.bind(this);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        requestPermissions();
        initView();
        initData();
        initListener();
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MsgEvent<DocParams> event) {
        if (event.type.equals("refresh_doc_detail")) {
            refresh();
        }
    }

    public void refresh() {
        finish();
        Bundle args = new Bundle();
        args.putSerializable("data", docRows);
        LActivityTool.startActivity(args, DocDetailActivity.class);
    }

    @Override
    public void initView() {
        if (getIntent().hasExtra("data")) {
            docRows = (DocRows) getIntent().getSerializableExtra("data");
        }
        if (getIntent().hasExtra("other")) {
            EventBus.getDefault().post(new MsgEvent<>("other", ""));
        }


        if (LEmptyTool.isNotEmpty(docRows)) {
            //收文
            if (docRows.documentType == DOC_TYPE_RECEIPT)
                fragments = new LinkedList<>(Arrays.asList(DocDetailPager.getInstance(docRows), FlowDocDetailPager.getInstance(docRows.documentId)));
            else
                fragments = new LinkedList<>(Arrays.asList(DocDetailPager.getInstance(docRows), FlowDocDetailPager.getInstance(docRows.documentId)));

            titles = new LinkedList<>(Arrays.asList("公文详情", "办理过程"));
            tabViewPager.setAdapter(new TbVPAdapter(getSupportFragmentManager(), fragments, titles));
            tabLayout.setViewPager(tabViewPager);
        }

    }

    @OnClick({R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.iv_collect)
    public void onViewClicked() {

        mApi.myGroupList(this, new JsonCallback<List<Group>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogTool.e(TAG, e);
            }

            @Override
            public void onResponse(List<Group> groups, int i) {

                GroupPop groupPop = new GroupPop(DocDetailActivity.this, groups);
                groupPop.showPopupWindow();
                groupPop.setOnClickListener(item -> {

                    mApi.addCollect(this, item.collectGroupId, docRows.documentId, new JsonCallback() {
                        @Override
                        public void onError(Call call, Exception e, int i) {
                            LogTool.e(TAG, e);
                        }

                        @Override
                        public void onResponse(Object o, int i) {
                            LToast.normal("收藏成功.");
                            groupPop.dismiss();
                        }
                    });
                });
            }
        });


    }

    private void requestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // Permission has not been granted and must be
                // requested.

                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                }
                requestPermissions(
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            } else {

            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }



}
