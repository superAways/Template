package com.zxl.template.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.zxl.template.R;
import com.zxl.template.TestActivity;
import com.zxl.template.base.BaseAty;
import com.zxl.zlibrary.tool.LActivityTool;
import com.zxl.zlibrary.tool.LStatusBarTool;

/**
 * Created by zxl.
 */
public class SplashActivity extends BaseAty {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        LStatusBarTool.immersive(this);

        new Handler().postDelayed(() -> {
            LActivityTool.startActivity(LoginActivity.class);
            finish();
        }, 1000);

    }

}
