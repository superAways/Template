package com.zxl.template.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;

import com.alibaba.fastjson.JSON;
import com.zxl.template.R;
import com.zxl.template.model.MsgEvent;
import com.zxl.template.model.Vpn;
import com.zxl.zlibrary.tool.LCacheTool;
import com.zxl.zlibrary.tool.LEmptyTool;
import com.zxl.zlibrary.tool.LToast;
import com.zxl.zlibrary.view.LClearEditText;
import com.zxl.zlibrary.view.LTitleBarView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/*
 * @created by LoveLing on 2018/12/16
 * @emil 1079112877@qq.com
 * description:
 */
public class VpnAty extends AppCompatActivity {
    @BindView(R.id.titleBar)
    LTitleBarView titleBar;
    @BindView(R.id.et_ip)
    LClearEditText etIp;
    @BindView(R.id.et_port)
    LClearEditText etPort;
    @BindView(R.id.et_username)
    LClearEditText etUsername;
    @BindView(R.id.et_password)
    LClearEditText etPassword;
    @BindView(R.id.btn_save)
    FancyButton btnSave;
    @BindView(R.id.checkBox)
    AppCompatCheckBox checkBox;

    private LCacheTool vpnConfig;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aty_vpn);
        ButterKnife.bind(this);

        titleBar.setLeftOnClickListener(v -> finish());


        vpnConfig = LCacheTool.getInstance("vpn_config");
        String vpnStr = vpnConfig.getString("vpn_config");
        if (LEmptyTool.isNotEmpty(vpnStr)) {
            Vpn vpn = JSON.parseObject(vpnStr, Vpn.class);
            etIp.setText(vpn.vpnIp);
            etPort.setText(vpn.vpnPort);
            etUsername.setText(vpn.vpnName);
            etPassword.setText(vpn.vpnPsd);
        }


        btnSave.setOnClickListener(v -> {
            String ip = etIp.getEditableText().toString();
            String port = etPort.getEditableText().toString();
            String username = etUsername.getEditableText().toString();
            String password = etPassword.getEditableText().toString();


            if (LEmptyTool.isEmpty(ip)) {
                etIp.setShakeAnimation();
                LToast.normal("请输入ip");
                return;
            }

            if (LEmptyTool.isEmpty(port)) {
                etPort.setShakeAnimation();
                LToast.normal("请输入端口");
                return;
            }

            if (LEmptyTool.isEmpty(username)) {
                etUsername.setShakeAnimation();
                LToast.normal("请输入账号");
                return;
            }

            if (LEmptyTool.isEmpty(password)) {
                etPassword.setShakeAnimation();
                LToast.normal("请输入密码");
                return;
            }


            Vpn vpn = new Vpn();

            vpn.vpnIp = ip;
            vpn.vpnPort = port;
            vpn.vpnName = username;
            vpn.vpnPsd = password;

            vpnConfig.put("vpn_config", JSON.toJSONString(vpn));
            LToast.normal("配置成功");
            if (checkBox.isChecked()) {
                EventBus.getDefault().post(new MsgEvent<>("vpn_login", ""));
            }
        });


    }
}
