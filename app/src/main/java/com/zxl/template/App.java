package com.zxl.template;

import android.app.Application;
import android.content.Intent;

import com.example.sslvpn_android_client.VPNServiceManager;
import com.luliang.shapeutils.DevShapeUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.tencent.bugly.Bugly;
import com.zxl.template.http.ApiControl;
import com.zxl.zlibrary.http.OkHttpUtils;
import com.zxl.zlibrary.tool.LEmptyTool;
import com.zxl.zlibrary.tool.LToast;
import com.zxl.zlibrary.tool.LTool;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import me.texy.treeview.base.SelectableTreeAction;
import okhttp3.OkHttpClient;

import static com.zxl.template.constant.constant.BROADCAST_VPN_SERVICE;

/*
 * @created by LoveLing on 2018/11/12
 * @emil 1079112877@qq.com
 * description:
 */
public class App extends Application {

    public static App mApp = null;
    public ApiControl mApi = null;

    private static String appID = "28e8ebd590";
    private static String noIconAppId = "b4ff978b25";

    public VPNServiceManager mVpn = null;


    //static 代码段可以防止内存泄露
    static {
        //设置全局的Header构建器
        SmartRefreshLayout.setDefaultRefreshHeaderCreator((context, layout) -> {
            layout.setPrimaryColorsId(R.color.colorPrimary, android.R.color.white);//全局设置主题颜色
            return new ClassicsHeader(context);//.setTimeFormat(new DynamicTimeFormat("更新于 %s"));//指定为经典Header，默认是 贝塞尔雷达Header
        });
        //设置全局的Footer构建器
        SmartRefreshLayout.setDefaultRefreshFooterCreator((context, layout) -> {
            //指定为经典Footer，默认是 BallPulseFooter
            return new ClassicsFooter(context).setDrawableSize(20);
        });
    }

    private Timer timer;


    @Override
    public void onCreate() {
        super.onCreate();


        init();
        Bugly.init(this, noIconAppId, true);
//        Bugly.init(this, appID, true);
    }

    private void init() {
        if (LEmptyTool.isEmpty(mApp)) {
            mApp = this;
        }
        if (mVpn == null) {
            mVpn = VPNServiceManager.getVpnServiceManager();

           /* new Thread(() -> {
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        int status = mVpn.getStatus();
                        //vpn状态
                        switch (status) {
                            //            服务开启
                            case VPNServiceManager.VPN_SERVICE_CONNECT_SUCCESS:

                                break;
                            //        服务关闭
                            case VPNServiceManager.VPN_SERVICE_CONNECT_CLOSED:
                                sendBroadcast(new Intent(BROADCAST_VPN_SERVICE));
                                break;
                            //服务已断开，正在重连
                            case VPNServiceManager.VPN_SERVICE_RECONNECT:
                                break;
                            default:

                                break;
                        }

                    }
                };
                if (LEmptyTool.isEmpty(timer))
                    timer = new Timer();
                timer.schedule(task, 0, 2000);
            }).start();*/
        }

        LTool.init(this);

        if (LEmptyTool.isEmpty(mApi)) {
            mApi = new ApiControl(getApplicationContext());
        }
        //shapeUtils
        DevShapeUtils.init(this);


        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(20000L, TimeUnit.MILLISECONDS)
                .readTimeout(20000L, TimeUnit.MILLISECONDS)
                //其他配置
                .build();

        OkHttpUtils.initClient(okHttpClient);

    }

}
