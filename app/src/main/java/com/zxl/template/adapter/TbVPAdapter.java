package com.zxl.template.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.zxl.template.base.BaseFrag;

import java.util.List;

/*
 * @created by LoveLing on 2018/11/20
 * @emil 1079112877@qq.com
 * description:
 */
public class TbVPAdapter extends FragmentPagerAdapter {

    private List<BaseFrag> mFragments;
    private List<String> mTitle;

    public TbVPAdapter(FragmentManager fm, List<BaseFrag> fragments) {
        super(fm);
        this.mFragments = fragments;
    }

    public TbVPAdapter(FragmentManager fm, List<BaseFrag> fragments, List<String> title) {
        super(fm);
        this.mFragments = fragments;
        this.mTitle = title;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitle.get(position);
    }

}
