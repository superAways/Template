package com.zxl.template.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.sslvpn_android_client.VPNServiceManager;
import com.yhy.widget.core.toggle.SwitchButton;
import com.zxl.template.App;
import com.zxl.template.R;
import com.zxl.template.activity.FollowAty;
import com.zxl.template.activity.LoginActivity;
import com.zxl.template.activity.MainActivity;
import com.zxl.template.base.BaseFrag;
import com.zxl.template.base.IActivity;
import com.zxl.zlibrary.tool.LActivityTool;
import com.zxl.zlibrary.tool.LCacheTool;
import com.zxl.zlibrary.tool.LEmptyTool;
import com.zxl.zlibrary.tool.LToast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/*
 * @created by LoveLing on 2018/11/20
 * @emil 1079112877@qq.com
 * description:
 */
public class SettingFrag extends BaseFrag implements IActivity {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_company)
    TextView tvCompany;
    @BindView(R.id.tv_office)
    TextView tvOffice;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_office_phone)
    TextView tvOfficePhone;
    @BindView(R.id.tv_server_setting)
    TextView tvServerSetting;
    @BindView(R.id.sb_vpn)
    SwitchButton sbVpn;
    Unbinder unbinder;

    public static BaseFrag getInstance() {
        return new SettingFrag();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_setting, container, false);
        unbinder = ButterKnife.bind(this, inflate);
        return inflate;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
        initListener();
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {
        tvName.setText(mApi.mCurrentUser.userCnName);
        tvCompany.setText(mApi.mCurrentUser.orgName);
        tvPhone.setText(mApi.mCurrentUser.cellponeTel);
        tvOfficePhone.setText(mApi.mCurrentUser.officeTel);
    }

    @Override
    public void initView() {



        LCacheTool cacheTool = LCacheTool.getInstance("http");
        String api = cacheTool.getString("api");
        if (LEmptyTool.isNotEmpty(api)) {
            tvServerSetting.setText(api);
        }

        sbVpn.setOnStateChangeListener((view, isChecked) -> {
            //是否开启动
            if (isChecked) {
                cacheTool.put("vpn", "1");
            } else {
                cacheTool.put("vpn", "0");
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.rl_follow, R.id.rl_server_setting, R.id.rl_about, R.id.logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_follow:
                LActivityTool.startActivity(FollowAty.class);
                break;
            case R.id.rl_server_setting:

                break;
            case R.id.rl_about:

                break;
            //退出登录
            case R.id.logout:
                mApi.mCurrentUser = null;
                LActivityTool.startActivity(LoginActivity.class);
//                LActivityTool.finishActivity(MainActivity.class);
                break;
            default:
                break;
        }
    }


}
