package com.zxl.template.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zxl.template.R;
import com.zxl.template.activity.DocDetailActivity;
import com.zxl.template.base.BaseFrag;
import com.zxl.template.base.IActivity;
import com.zxl.template.http.JsonCallback;
import com.zxl.template.model.Collect;
import com.zxl.template.model.CollectRow;
import com.zxl.template.model.DocRows;
import com.zxl.template.model.Group;
import com.zxl.zlibrary.tool.LActivityTool;
import com.zxl.zlibrary.tool.LEmptyTool;
import com.zxl.zlibrary.tool.LogTool;
import com.zxl.zlibrary.view.LTitleBarView;
import com.zxl.zlibrary.view.statusview.LStatusView;

import org.angmarch.views.NiceSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.Call;

/*
 * @created by LoveLing on 2018/11/20
 * @emil 1079112877@qq.com
 * description:收藏
 */
public class NCollectFrag extends BaseFrag implements IActivity {
    private static final String TAG = "NCollectFrag";

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    Unbinder unbinder;
    @BindView(R.id.nice_spinner)
    NiceSpinner niceSpinner;

    List<Group> groupList = new ArrayList<>();
    @BindView(R.id.titleBarView)
    LTitleBarView titleBarView;
    @BindView(R.id.status_view)
    LStatusView statusView;


    private String groupId;

    public static BaseFrag getInstance() {
        return new NCollectFrag();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_collect_n, container, false);
        unbinder = ButterKnife.bind(this, inflate);
        return inflate;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
        initListener();
    }

    @Override
    public void initListener() {
        niceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (LEmptyTool.isNotEmpty(groupList)) {
                    groupId = groupList.get(position).collectGroupId;
                    initNet();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        titleBarView.setRightTextOnClickListener(v -> initData());
        statusView.setOnViewRefreshListener(this::initNet);

    }


    @Override
    public void initData() {
        mApi.myGroupList(this, new JsonCallback<List<Group>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogTool.e(TAG, e);
            }

            @Override
            public void onResponse(List<Group> groups, int i) {
                if (groups.size() > 0) {
                    groupId = groups.get(0).collectGroupId;

                    groupList.addAll(groups);
                    niceSpinner.attachDataSource(groups);

                    initNet();
                }

            }
        });

    }

    private void initNet() {
        statusView.onLoadingView();
        mApi.collectListJson(this, groupId, new JsonCallback<Collect>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogTool.e(TAG, e);
                if (LEmptyTool.isNotEmpty(statusView))
                    statusView.onErrorView();
            }

            @Override
            public void onResponse(Collect collect, int i) {
                if (LEmptyTool.isNotEmpty(collect.collectRows)) {
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    RvAdapter rvAdapter = new RvAdapter(collect.collectRows);
                    recyclerView.setAdapter(rvAdapter);

                    rvAdapter.setOnItemClickListener((adapter, view, position) -> {
                        CollectRow collectRow = (CollectRow) adapter.getItem(position);

                        DocRows docRows = new DocRows();
                        assert collectRow != null;
                        docRows.documentId = collectRow.documentId;
                        docRows.documentType = collectRow.documentType;

                        Bundle args = new Bundle();
                        args.putSerializable("data", docRows);
                        LActivityTool.startActivity(args, DocDetailActivity.class);
                    });
                }
                statusView.onSuccessView();
            }
        });
    }

    @Override
    public void initView() {

    }

    class RvAdapter extends BaseQuickAdapter<CollectRow, BaseViewHolder> {

        public RvAdapter(@Nullable List<CollectRow> data) {
            super(R.layout.item_doc_collect, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, CollectRow item) {
            helper.setText(R.id.tv_title, item.title);
            helper.setText(R.id.tv_doc_type, item.documentType == 1 ? " 收文 " : "发文");
            helper.setText(R.id.tv_name, item.fromUnit);
            helper.setText(R.id.tv_time, item.createTime);

            if (item.documentType == 1) {
                helper.setBackgroundRes(R.id.tv_doc_type, R.color.colorPrimary);
            } else {
                helper.setBackgroundRes(R.id.tv_doc_type, R.color.textGreen);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
