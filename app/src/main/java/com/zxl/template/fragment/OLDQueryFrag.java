package com.zxl.template.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.luliang.shapeutils.DevShapeUtils;
import com.luliang.shapeutils.shape.DevShape;
import com.yhy.widget.layout.flow.tag.TagFlowAdapter;
import com.yhy.widget.layout.flow.tag.TagFlowLayout;
import com.zxl.template.R;
import com.zxl.template.adapter.TbVPAdapter;
import com.zxl.template.base.BaseFrag;
import com.zxl.template.base.IActivity;
import com.zxl.template.view.pop.DocSearchPop;
import com.zxl.zlibrary.tool.LToast;
import com.zxl.zlibrary.view.LClearEditText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.zxl.template.constant.constant.PAGE_QUERY;

/*
 * @created by LoveLing on 2018/11/20
 * @emil 1079112877@qq.com
 * description:查询
 */
public class OLDQueryFrag extends BaseFrag implements IActivity {
    private static final String TAG = "QueryFrag";

    private Unbinder unbinder;

    @BindView(R.id.rl_title_bar)
    RelativeLayout rlTitleBar;

    @BindView(R.id.tabLayout)
    SlidingTabLayout tabLayout;
    @BindView(R.id.tv_search)
    TextView tvSearch;

    @BindView(R.id.view_pager)
    ViewPager tabViewPager;
    @BindView(R.id.ll_history)
    LinearLayout llHistory;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.tfl_history)
    TagFlowLayout tflHistory;
    @BindView(R.id.et_keyword)
    LClearEditText etKeyword;

    private List<String> historyList = new ArrayList<>();
    private TagAdapter tagAdapter;


    public static BaseFrag getInstance() {
        return new OLDQueryFrag();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_query, container, false);
        unbinder = ButterKnife.bind(this, inflate);
        return inflate;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
        initListener();
    }

    @Override
    public void initListener() {
        tflHistory.setOnCheckChangedListener((checked, position, data, dataList) -> {
            etKeyword.setText((String) data);
            etKeyword.setSelection(etKeyword.getText().length());
            search();
        });
    }


    @OnClick({R.id.iv_search, R.id.tv_search, R.id.iv_delete})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_search:
                DocSearchPop pop = new DocSearchPop(getActivity(), rlTitleBar);
                pop.setOnQueryClickListener((docType, title, content, yearNumber, issueNumber, startDate, endDate) -> {
                    LToast.normal(docType + "~~" + title + "~~" + content + "~~" + yearNumber + "~~" + issueNumber + "~~" + startDate + "~~" + endDate);
                });

                pop.toggle();

                LToast.normal("高级搜索");
                break;

            case R.id.iv_delete:
                LToast.normal("删除历史记录");
                historyList.clear();
                tagAdapter.notifyDataChanged();
                break;

            case R.id.tv_search:
                LToast.normal("查询");
                search();
                break;
            default:

                break;
        }
    }

    @Override
    public void initData() {
        historyList.clear();
        historyList.add("公路服务设施");
        historyList.add("意见稿");
        historyList.add("关于  什么的意见");
        historyList.add("设计规范");

        tagAdapter.notifyDataChanged();


    }

    @Override
    public void initView() {
        llContent.setVisibility(View.GONE);
        llHistory.setVisibility(View.VISIBLE);
        List<String> titles = new LinkedList<>(Arrays.asList("收文", "发文"));
        List<BaseFrag> fragments = new LinkedList<>(Arrays.asList(DocPager.getInstance(PAGE_QUERY, 3), DocPager.getInstance(PAGE_QUERY, 3)));

        tabViewPager.setAdapter(new TbVPAdapter(getChildFragmentManager(), fragments, titles));
        tabLayout.setViewPager(tabViewPager);

        tagAdapter = new TagAdapter(historyList);
        tflHistory.setAdapter(tagAdapter);

        DevShapeUtils.shape(DevShape.RECTANGLE).solid(R.color.white).line(1, R.color.colorLine).radius(16).into(etKeyword);
        DevShapeUtils.shape(DevShape.RECTANGLE).solid(R.color.colorPrimary).line(1, R.color.colorLine).radius(16).into(tvSearch);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void search() {
        if (true) {//有数据
            llHistory.setVisibility(View.GONE);
            llContent.setVisibility(View.VISIBLE);
        }
    }

    private class TagAdapter extends TagFlowAdapter<String> {

        public TagAdapter(List<String> dataList) {
            super(dataList);
        }

        @Override
        public View getView(TagFlowLayout parent, int position, String data) {
            TextView tv = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.item_search_tag_flow, null);
            DevShapeUtils.shape(DevShape.RECTANGLE).solid(R.color.windowBackground).radius(8).into(tv);
            tv.setText(data);
            return tv;
        }
    }
}
