package com.zxl.template.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zxl.template.R;
import com.zxl.template.base.BaseFrag;
import com.zxl.template.base.IActivity;
import com.zxl.template.http.JsonCallback;
import com.zxl.template.model.FlowDetail;
import com.zxl.zlibrary.tool.LEmptyTool;
import com.zxl.zlibrary.tool.LogTool;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.Call;

/**
 * author : 王新海
 * time   : 2018/11/21.
 * version: 1.0.0
 * desc   : 流转详情列表
 */
public class FlowDocDetailPager extends BaseFrag implements IActivity {

    private static final String TAG = "FlowDocDetailPager";

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private Unbinder unbinder;
    private String docId;


    public static BaseFrag getInstance(String docId) {
        FlowDocDetailPager flowDocDetailPager = new FlowDocDetailPager();
        Bundle args = new Bundle();
        args.putString("id", docId);
        flowDocDetailPager.setArguments(args);
        return flowDocDetailPager;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.tab_flow_doc_detail, container, false);
        unbinder = ButterKnife.bind(this, inflate);
        return inflate;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
        initListener();
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {
        assert getArguments() != null;
        docId = getArguments().getString("id");
        initNet();
    }

    private void initNet() {
        mApi.flowDetail(this, docId, new JsonCallback<List<FlowDetail>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogTool.e(TAG, e);
            }


            @Override
            public void onResponse(List<FlowDetail> flowDetails, int i) {

                assert flowDetails != null;
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                recyclerView.setAdapter(new RvAdapter(flowDetails));
            }
        });

    }

    @Override
    public void initView() {

    }

    class RvAdapter extends BaseQuickAdapter<FlowDetail, BaseViewHolder> {

        public RvAdapter(@Nullable List<FlowDetail> data) {
            super(R.layout.item_flow_detial, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, FlowDetail item) {
            helper.setText(R.id.tv_title, item.title + "(操作：" + item.userCnName + ")");

            if (LEmptyTool.isEmpty(item.endTime)) {
                helper.setText(R.id.tv_opinion, "暂未办结.");
            } else {
                helper.setText(R.id.tv_opinion, item.opinion + "(办理结束：" + item.endTime + ")");
            }
            helper.setText(R.id.tv_time, "发送：" + item.sender + "  " + item.createTime);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        initNet();
    }

    @Override
    public void onStart() {
        super.onStart();
        initNet();
    }
}
