package com.zxl.template.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.widget.MsgView;
import com.zxl.template.R;
import com.zxl.template.adapter.TbVPAdapter;
import com.zxl.template.base.BaseFrag;
import com.zxl.template.base.IActivity;
import com.zxl.template.constant.constant;
import com.zxl.template.http.JsonCallback;
import com.zxl.template.model.Doc;
import com.zxl.template.model.DocParams;
import com.zxl.template.model.MsgEvent;
import com.zxl.template.view.pop.DocParamsPop;
import com.zxl.template.view.pop.DocPop;
import com.zxl.zlibrary.tool.LEmptyTool;
import com.zxl.zlibrary.tool.LToast;
import com.zxl.zlibrary.tool.LogTool;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.Call;

/*
 * @created by LoveLing on 2018/11/20
 * @emil 1079112877@qq.com
 * description:公文
 */
public class DocFrag extends BaseFrag implements IActivity, constant {
    private static final String TAG = "DocFrag";
    @BindView(R.id.rl_toolbar)
    RelativeLayout rlToolbar;
    private Unbinder unbinder;
    @BindView(R.id.tabLayout)
    SlidingTabLayout tabLayout;
    @BindView(R.id.iv_menu)
    ImageView ivMenu;

    @BindView(R.id.view_pager)
    ViewPager tabViewPager;

    private DocPop mDocPop;

    private List<DocParams> mDocParams = new ArrayList<>();

    //已经办理
    private List<DocParams> docParamsList = new ArrayList<>();


    public static BaseFrag getInstance() {
        return new DocFrag();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_doc, container, false);
        unbinder = ButterKnife.bind(this, inflate);
        return inflate;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
        initView();
        initData();
        initListener();
    }

    @Override
    public void initListener() {

    }

    @OnClick({R.id.iv_menu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_menu:
                if (tabLayout.getCurrentTab() == 0) {
                    if (LEmptyTool.isNotEmpty(mDocParams)) {

                        DocParamsPop docParamsPop = new DocParamsPop(getContext(), mDocParams, 1);
                        docParamsPop.showPopupWindow(rlToolbar);

                        //
                        docParamsPop.SetOnItemSelectOnClickListener(docParams -> {
                            EventBus.getDefault().post(new MsgEvent<>(EVENTBUS_AGENCY_DOC_REGRESH_LIST, docParams));
                            docParamsPop.dismiss();
                        });

                        docParamsPop.setOnAllOnClick(() -> {
                            EventBus.getDefault().post(new MsgEvent<>(EVENTBUS_AGENCY_DOC_REGRESH_ALL, ""));
                            docParamsPop.dismiss();
                        });
                    }
                } else if (tabLayout.getCurrentTab() == 1) {
                    if (LEmptyTool.isEmpty(docParamsList)) {
                        DocParams docParams = new DocParams();
                        docParams.flowName = "收文";
                        docParams.flowId = "1";
                        docParamsList.add(docParams);


                        docParams = new DocParams();
                        docParams.flowName = "发文";
                        docParams.flowId = "2";
                        docParamsList.add(docParams);

                        docParams = new DocParams();
                        docParams.flowName = "我的拟文";
                        docParams.flowId = "3";
                        docParamsList.add(docParams);

                        docParams = new DocParams();
                        docParams.flowName = "已办结";
                        docParams.flowId = "5";
                        docParamsList.add(docParams);


                        docParams = new DocParams();
                        docParams.flowName = "未办结";
                        docParams.flowId = "6";
                        docParamsList.add(docParams);


                        docParams = new DocParams();
                        docParams.flowName = "已挂起";
                        docParams.flowId = "4";
                        docParamsList.add(docParams);
                    }

                    DocParamsPop docParamsPop = new DocParamsPop(getContext(), docParamsList, 2);
                    docParamsPop.showPopupWindow(rlToolbar);

                    //
                    docParamsPop.SetOnItemSelectOnClickListener(item -> {
                        EventBus.getDefault().post(new MsgEvent<>(EVENTBUS_DO_DOC_REGRESH_LIST, item));
                        docParamsPop.dismiss();
                    });

                    docParamsPop.setOnAllOnClick(() -> {
                        EventBus.getDefault().post(new MsgEvent<>(EVENTBUS_DO_DOC_REGRESH_ALL, ""));
                        docParamsPop.dismiss();
                    });

                }
                break;
            default:

                break;
        }
    }

    @Override
    public void initData() {
        mApi.todoListParamList(this, new JsonCallback<List<DocParams>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogTool.e(TAG, e);
            }

            @Override
            public void onResponse(List<DocParams> docParams, int i) {
                mDocParams.clear();
                if (!docParams.isEmpty()) {
                    mDocParams.addAll(docParams);
                    Iterator<DocParams> iterator = mDocParams.iterator();
                    while (iterator.hasNext()) {
                        DocParams next = iterator.next();
                        if (next.nums <= 0) {
                            iterator.remove();
                        }
                    }
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MsgEvent<DocParams> event) {
        if (event.type.equals("refresh_doc_list")) {
//            tabLayout.setCurrentTab(1);
        }
    }

    @Override
    public void initView() {


        List<String> titles = new LinkedList<>(Arrays.asList("待办公文", "已办公文"));
        List<BaseFrag> fragments = new LinkedList<>(Arrays.asList(AgencyDocPager.getInstance()
                , DoDocPager.getInstance()));

        tabViewPager.setAdapter(new TbVPAdapter(getChildFragmentManager(), fragments, titles));
        tabLayout.setViewPager(tabViewPager);

        /*mApi.docList(this, 1, new JsonCallback<Doc>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogTool.e(TAG, e);
            }

            @Override
            public void onResponse(Doc doc, int i) {
                tabLayout.showMsg(0, doc.total);
            }
        });

        mApi.docList(this, 2, new JsonCallback<Doc>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogTool.e(TAG, e);
            }

            @Override
            public void onResponse(Doc doc, int i) {
                tabLayout.showMsg(1, doc.total);
            }
        });
*/
        tabLayout.setMsgMargin(0, -5, 5);
        MsgView msgView = tabLayout.getMsgView(0);
        if (msgView != null) {
            msgView.setBackgroundColor(Color.parseColor("#007DFE"));
        }


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();


        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }
}
