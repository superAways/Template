package com.zxl.template.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.luliang.shapeutils.DevShapeUtils;
import com.luliang.shapeutils.shape.DevShape;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.zxl.template.R;
import com.zxl.template.activity.DocDetailActivity;
import com.zxl.template.base.BaseFrag;
import com.zxl.template.base.IActivity;
import com.zxl.template.http.JsonCallback;
import com.zxl.template.model.Doc;
import com.zxl.template.model.DocRows;
import com.zxl.zlibrary.tool.LActivityTool;
import com.zxl.zlibrary.tool.LogTool;
import com.zxl.zlibrary.view.LClearEditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.Call;

/**
 * author : 王新海
 * time   : 2018/11/21.
 * version: 1.0.0
 * desc   :
 */
public class DocPager extends BaseFrag implements IActivity {
    private static final String TAG = "DocPager";
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;


    private Unbinder unbinder;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.tv_search)
    TextView tvSearch;

    @BindView(R.id.ll_search)
    LinearLayout llSearch;

    @BindView(R.id.et_search)
    LClearEditText etSearch;
    private RvAdapter rvAdapter;
    private String mPageType;
    private int mNetType;
    private int mPage = 1;
    private int mRows = 16;
    private String key;

    private List<DocRows> rowsList = new ArrayList<>();

    public static BaseFrag getInstance(String pageType, int netType) {
        DocPager pager = new DocPager();
        Bundle b = new Bundle();
        b.putString("type", pageType);
        b.putInt("netType", netType);
        pager.setArguments(b);
        return pager;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.tab_doc_pager, container, false);
        unbinder = ButterKnife.bind(this, inflate);
        return inflate;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
        initListener();
    }

    @Override
    public void initView() {
        DevShapeUtils.shape(DevShape.RECTANGLE).solid(R.color.white).line(1, R.color.colorLine).radius(16).into(etSearch);
        DevShapeUtils.shape(DevShape.RECTANGLE).solid(R.color.colorPrimary).line(1, R.color.colorLine).radius(16).into(tvSearch);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvAdapter = new RvAdapter(rowsList);
        recyclerView.setAdapter(rvAdapter);

    }

    @Override
    public void initListener() {
        rvAdapter.setOnItemClickListener((adapter, view, position) -> {
            DocRows item = (DocRows) adapter.getData().get(position);
            Bundle args = new Bundle();
            args.putSerializable("data", item);
            LActivityTool.startActivity(args, DocDetailActivity.class);
        });

        refreshLayout.setOnRefreshListener(refreshLayout -> {
            initNet();
            refreshLayout.finishRefresh(2000);
        });
        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            refreshLayout.finishLoadMore(2000);
        });
    }

    @Override
    public void initData() {
        assert getArguments() != null;
        mPageType = getArguments().getString("type");
        mNetType = getArguments().getInt("netType");

        if (!"DOC".equals(mPageType)) {
            llSearch.setVisibility(View.GONE);
        }

        initNet();

    }

    private void initNet() {

    }


    class RvAdapter extends BaseQuickAdapter<DocRows, BaseViewHolder> {
        //默认代办公文
        int type = 1;

        public void setType(int type) {
            this.type = type;
        }

        RvAdapter(List<DocRows> data) {
            super(R.layout.item_doc_rv, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, DocRows item) {

            switch (type) {
                //代办理
                case 1:


                    break;
                //已经办理
                case 2:
                    helper.setText(R.id.tv_title, item.title);
                    helper.setText(R.id.tv_doc_type, item.documentType == 1 ? " 收文 " : "发文");
                    helper.setText(R.id.tv_name, item.sender);
                    helper.setText(R.id.tv_task, item.gwzh == null ? "" : item.gwzh);
                    helper.setText(R.id.tv_handle, item.handleName);
                    helper.setText(R.id.tv_time, item.createTime);
                    break;
            }

        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        LogTool.d(TAG + "unbinder.unbind();");
    }
}
