package com.zxl.template.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.luliang.shapeutils.DevShapeUtils;
import com.luliang.shapeutils.shape.DevShape;
import com.zxl.template.R;
import com.zxl.template.adapter.TbVPAdapter;
import com.zxl.template.base.BaseFrag;
import com.zxl.template.base.IActivity;
import com.zxl.template.view.pop.DocSearchPop;
import com.zxl.zlibrary.tool.LToast;
import com.zxl.zlibrary.view.LClearEditText;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.zxl.template.constant.constant.PAGE_COLLECT;

/*
 * @created by LoveLing on 2018/11/20
 * @emil 1079112877@qq.com
 * description:收藏
 */
public class CollectFrag extends BaseFrag implements IActivity {
    private static final String TAG = "CollectFrag";

    private Unbinder unbinder;

    @BindView(R.id.tabLayout)
    SlidingTabLayout tabLayout;
    @BindView(R.id.tv_search)
    TextView tvSearch;
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.et_keyword)
    LClearEditText etKeyword;

    @BindView(R.id.view_pager)
    ViewPager tabViewPager;


    public static BaseFrag getInstance() {
        return new CollectFrag();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_collect, container, false);
        unbinder = ButterKnife.bind(this, inflate);
        return inflate;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
        initListener();
    }

    @Override
    public void initListener() {

    }

    @OnClick({R.id.iv_search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_search:
                DocSearchPop pop = new DocSearchPop(getActivity(), ivSearch);
                pop.setOnQueryClickListener((docType, title, content, yearNumber, issueNumber, startDate, endDate) -> {
                    LToast.normal(docType + "~~" + title + "~~" + content + "~~" + yearNumber + "~~" + issueNumber + "~~" + startDate + "~~" + endDate);
                });

                pop.toggle();

                LToast.normal("高级搜索");
                break;

            default:

                break;
        }
    }

    @Override
    public void initData() {

    }

    @Override
    public void initView() {
        List<String> titles = new LinkedList<>(Arrays.asList("收文", "发文"));
        List<BaseFrag> fragments = new LinkedList<>(Arrays.asList(DocPager.getInstance(PAGE_COLLECT, 4), DocPager.getInstance(PAGE_COLLECT, 4)));

        tabViewPager.setAdapter(new TbVPAdapter(getChildFragmentManager(), fragments, titles));
        tabLayout.setViewPager(tabViewPager);

        DevShapeUtils.shape(DevShape.RECTANGLE).solid(R.color.white).line(1, R.color.colorLine).radius(16).into(etKeyword);
        DevShapeUtils.shape(DevShape.RECTANGLE).solid(R.color.colorPrimary).line(1, R.color.colorLine).radius(16).into(tvSearch);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
